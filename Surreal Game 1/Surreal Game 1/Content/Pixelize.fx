﻿sampler inputTexture;
int pixelation;
int width;
int height;

float4 MainPS(float2 pixCoords: TEXCOORD0) : COLOR0
{
	pixCoords *= float2(width, height);
	float2 newCoords;
	newCoords.x = round(pixCoords.x / pixelation) * pixelation;
	newCoords.y = round(pixCoords.y / pixelation) * pixelation;
	newCoords /= float2(width, height);

	return tex2D(inputTexture, newCoords);
}

technique Technique1
{
	pass Pass1
	{
		PixelShader = compile ps_3_0 MainPS();
		AlphaBlendEnable = TRUE;
		DestBlend = INVSRCALPHA;
		SrcBlend = SRCALPHA;
	}
};