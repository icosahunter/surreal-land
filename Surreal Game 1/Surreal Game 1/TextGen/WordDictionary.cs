﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{    
    public struct Noun
    {
        public Noun(string singular_, string plural_)
        {
            singular = singular_;
            plural = plural_;
        }
        public string singular;
        public string plural;
    }

    public struct Verb
    {
        public Verb(string presentTense_, string pastTense_, string pastParticiple_, string infinitive_)
        {
            presentTense = presentTense_;
            pastTense = pastTense_;
            pastParticiple = pastParticiple_;
            infinitive = infinitive_;
        }
        public string presentTense;
        public string pastTense;
        public string pastParticiple;
        public string infinitive;
    }
    public enum VerbTense { PRESENT_TENSE, PAST_TENSE, PAST_PARTICIPLE, INFINITIVE };

    class WordDictionary
    {
        public List<string> adjectives = new List<string>();
        public List<string> adverbs = new List<string>();
        public List<Noun> nouns = new List<Noun>();
        public List<Verb> verbs = new List<Verb>();

        public string RandomVerb(VerbTense tense = VerbTense.PRESENT_TENSE)
        {
            switch(tense)
            {
                case VerbTense.PRESENT_TENSE:
                    return verbs.Random().presentTense;
                case VerbTense.PAST_TENSE:
                    return verbs.Random().pastTense;
                case VerbTense.PAST_PARTICIPLE:
                    return verbs.Random().pastParticiple;
                case VerbTense.INFINITIVE:
                    return verbs.Random().infinitive;
                default:
                    return "";
            }
        }

        public string RandomNoun(bool plural = false)
        {
            if (plural)
            {
                return nouns.Random().plural;
            }
            else
            {
                return nouns.Random().singular;
            }
        }

        public string RandomAdjective()
        {
            return adjectives.Random();
        }

        public string RandomAdverb()
        {
            return adverbs.Random();
        }
    }
}
