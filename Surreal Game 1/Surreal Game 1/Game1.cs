﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Surreal_Game_1.GUI;

namespace Surreal_Game_1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        Window win;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            InputController.Assign(InteractionTypes.JUMP, Keys.Space);
            InputController.Assign(InteractionTypes.MOVE_LEFT, Keys.Left);
            InputController.Assign(InteractionTypes.MOVE_RIGHT, Keys.Right);
            InputController.Assign(InteractionTypes.MOVE_UP, Keys.Up);
            InputController.Assign(InteractionTypes.CLICK, Inputs.MouseButton.LEFT);
            InputController.Assign(InteractionTypes.ALT_CLICK, Inputs.MouseButton.RIGHT);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            graphics.ApplyChanges();
            win = new Window(this, graphics);
            win.width = 1792;
            win.height = 900;
            GameScreen screen = new GameScreen(win);
            win.currentScreen = screen;
            DefaultContent.XTile = new Tile(new Image(Content.Load<Texture2D>("redBox")));
            DefaultContent.OutlineBlock = new Image(Content.Load<Texture2D>("OutlineSquare"));
            DefaultContent.BlankTexture = Content.Load<Texture2D>("whiteSquare");
            Image fontimage = new Image(Content.Load<Texture2D>("font-simple_5x7"));
            ImageFont myfont = new ImageFont();
            myfont.LoadFromSpriteSheet("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]\\^_`abcdefghijklmnopqrstuvwxyz{|}~", fontimage, new Point(6, 8));

            List<Image> bonusTiles = new List<Image>();
            List<Image> sampleTiles = new List<Image>();

            for(int i = 1; i <= 7; i++)
            {
                bonusTiles.Add(new Image(Content.Load<Texture2D>("Test/bonus-tile-" + i.ToString())));
            }

            for (int i = 2; i <= 20; i++)
            {
                sampleTiles.Add(new Image(Content.Load<Texture2D>("Test/sample-tile-" + i.ToString())));
            }

            List<Tile> tiles = new List<Tile>();
            foreach (Image i in sampleTiles)
            {
                tiles.Add(new Tile(i, new Material(0.5, 0, 1, 1), new bool[] { true, true, true, true }));
            }
            /*
            int[,] tilear = TerrainGenerator.Caverns(new Point(45, 22), new int[] { 0, 1 }, new int[] { 1, 1 });
            int[,] brdr = TerrainGenerator.Border(new Point(45, 22), 1, new bool[] { true, true, true, true });
            tilear = TerrainGenerator.Composite(tilear, brdr);
            //int[,] tilear = TerrainGenerator.Layers(new Point(45, 22), new int[] { 0, 1 }, new int[] { 3, 1 }, 0.3, 2);
            */
            int[,] tilear = TerrainGenerator.Border(new Point(45, 22), 1, new bool[] { true, true, true, true });
            int[] mask = { 1, 12, 13, 8, 14, 3, 9, 5, 15, 11, 2, 4, 10, 7, 6, 16 };
            tilear = TerrainGenerator.BitMask(tilear, mask);

            Map map = new Map(tilear, tiles, new Point(40, 40));

            System.Diagnostics.Debug.Write(map.GetTileBounds(44, 0).X + "\n");

            Room room = new Room(screen, map, "test");

            screen.currentRoom = room;

            UIOverlay overlay = new UIOverlay(myfont, 25);
            DialogueController.currentMessage = "Hello World!";
            screen.AddChild(overlay);

            /*for (int i = 0; i < 5; i++)
            {
                Entity c = new Entity(new Point(300, 150), new Image(Content.Load<Texture2D>("Blue")), new Material(0.2, 0.9, 0.1, 0.2), new Point(40, 40), "boop boop");
                c.XSpeed = 10;
                c.YSpeed = 5;
                room.AddGameElement(c);
            }*/
            Player p = new Player(new Point(500, 175), new Image(Content.Load<Texture2D>("Yellow")), new Material(1, 0, 1, 0.5), new Point(40, 40), "Beep Beep");
            p.horizontalMoveSpeed = 1;
            p.verticalMoveSpeed = 25;
            p.canJump = true;
            p.maxSpeed = 30;
            // NOTE: A HITBOX WIDTH OF 1 WILL COVER A MULTITUDE OF PHYSICS SINS UNTIL WE FULLY IMPLEMENT TRULY CLEAN AND GOOD COLLISION PREVENTION!
            p.hitbox.SetHitWidth(1);
            room.AddGameElement(p);

            Entity a = new Entity(new Point(300, 175), new Image(Content.Load<Texture2D>("beachball")), new Material(1, 0.5, 1, 0.5), new Point(80, 80), "Hi! I'm a beachball!");
            a.AddTrigger(new Trigger(TriggerConditions.PlayerNearby(), TriggerResults.SendMessageToDialogueBoxUsingPlayerProximityPriority("Hi!, I'm a beachball!", 1000)));
            room.AddGameElement(a);

            Button mybtn = new Button(new Image(Content.Load<Texture2D>("blue")), new Point(800, 20), new Point(100, 80), click); ;
            screen.AddChild(mybtn);

            this.IsMouseVisible = true;
        }

        private void click(Trigger trigger, object owner, object other)
        {
            DialogueController.SendMessage("You pressed a button!", 2, 2000);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {

        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            win.Update();
			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			win.Draw();
			base.Draw(gameTime);
		}
	}
}
