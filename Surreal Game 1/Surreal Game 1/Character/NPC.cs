﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Surreal_Game_1
{
    public class NPC : Character
    {
        // Note: it is not recommended to set a movement AI unless you want two NPCs to have identical behaviour.
        public MovementAI movementAI;
        public int awarenessDistance;
        public NPC(Hitbox _hitbox, bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, Result _result, string _description, int _health, float _verticalMoveSpeed, float _horizontalMoveSpeed, int _maxVelocity, bool _facingRight, bool _blindsight) : base(_hitbox, _isSolid, _position, defaultImage, _material, _size, _result, _description, _health, _verticalMoveSpeed, _horizontalMoveSpeed, _maxVelocity, _facingRight, _blindsight)
        {
            movementAI = new MovementAI();
        }

        public NPC(Hitbox _hitbox, bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, Result _result, string _description, int _health, float _verticalMoveSpeed, float _horizontalMoveSpeed, int _maxVelocity, bool _facingRight, bool _blindsight, MoveRate _moveRate) : base(_hitbox, _isSolid, _position, defaultImage, _material, _size, _result, _description, _health, _verticalMoveSpeed, _horizontalMoveSpeed, _maxVelocity, _facingRight, _blindsight)
        {
            movementAI = new MovementAI();
        }

		public NPC(bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_isSolid, _position, defaultImage, _material, _size, _description)
        {
            movementAI = new MovementAI();
        }

		public NPC(Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_position, defaultImage, _material, _size, _description)
        {
            movementAI = new MovementAI();
        }

		public NPC(Point _position, IUIElement defaultImage, Point _size, string _description) : base(_position, defaultImage, _size, _description)
        {
            movementAI = new MovementAI();
        }

		public NPC(IUIElement defaultImage, Point _size, string _description) : base(defaultImage, _size, _description)
        {
            movementAI = new MovementAI();
        }

        public override void Update()
        {
            MoveType moveType = movementAI.DetermineMovement(position);

            if (moveType == MoveType.STOP)
            {
                XSpeed = 0;
            }
            else if (moveType == MoveType.LEFT)
            {
                if (velocity.X > 0)
                    XSpeed = 0;

                facingDirection = Math.PI;
                moveLeft = true;
            }
            else if (moveType == MoveType.RIGHT)
            {
                if (velocity.X < 0)
                    XSpeed = 0;

                facingDirection = 0;
                moveRight = true;
            }

            base.Update();
        }
    }
}
