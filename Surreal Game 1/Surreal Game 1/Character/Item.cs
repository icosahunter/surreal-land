﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
 
namespace Surreal_Game_1
{
    public class Item : UIElement
    {
        public IUIElement icon;
        public string name;
        public List<string> tags;
        public string description;
        public Dictionary<string, object> attributes = new Dictionary<string, object>();

        public Item(IUIElement img, string name_, string description_, params string[] tags_)
        {
            icon = img;
            name = name_;
            AddChild(icon);
            autoFormatting = true;
            tags = tags_.ToList();
            description = description_;
        }
    }
}
