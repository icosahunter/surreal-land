﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Player : Character
    {
        //public Player(Hitbox _hitbox, bool _isSolid, Point _position, IImage defaultImage, Material _material, Point _size, Result _result, string _description, int _health, float _verticalMoveSpeed, float _horizontalMoveSpeed, int _maxVelocity, bool _facingRight, bool _blindsight) : base(_hitbox, _isSolid, _position, defaultImage, _material, _size, _result, _description, _health, _verticalMoveSpeed, _horizontalMoveSpeed, _maxVelocity, _facingRight, _blindsight)
        //{
        //    return (triggerOwner != otherObject && triggerOwner is Player && otherObject is InputController && ((InputController)otherObject).IsButtonDown(InteractionTypes.MOVE_UP));
        //};
        //private Condition upC = delegate (object triggerOwner)
        //{
        //    return (triggerOwner != otherObject && triggerOwner is Player && otherObject is InputController && ((InputController)otherObject).IsButtonDown(InteractionTypes.MOVE_UP));
        //};
        //private Condition leftC = delegate (object triggerOwner)
        //{
        //    return (triggerOwner != otherObject && triggerOwner is Player && otherObject is InputController && ((InputController)otherObject).IsButtonDown(InteractionTypes.MOVE_LEFT));
        //};
        //private Condition rightC = delegate (object triggerOwner)
        //{
        //    return (triggerOwner != otherObject && triggerOwner is Player && otherObject is InputController && ((InputController)otherObject).IsButtonDown(InteractionTypes.MOVE_RIGHT));
        //};
        //private Condition jumpC = delegate (object triggerOwner)
        //{
        //    return (triggerOwner != otherObject && triggerOwner is Player && otherObject is InputController && ((InputController)otherObject).IsButtonDown(InteractionTypes.JUMP));
        //};

        Result upR = delegate (Trigger t, object triggerOwner, object otherObject)
        {
            ((Player)triggerOwner).moveUp = true;
        };
        Result leftR = delegate (Trigger t, object triggerOwner, object otherObject)
        {
            ((Player)triggerOwner).moveLeft = true;
        };
        Result rightR = delegate (Trigger t, object triggerOwner, object otherObject)
        {
            ((Player)triggerOwner).moveRight = true;
        };
        Result jumpR = delegate (Trigger t, object triggerOwner, object otherObject)
        {
             ((Player)triggerOwner).moveUp = true;
        };

        public Player(Hitbox _hitbox, bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, Result _result, string _description, int _health, float _verticalMoveSpeed, float _horizontalMoveSpeed, int _maxVelocity, bool _facingRight, bool _blindsight) : base(_hitbox, _isSolid, _position, defaultImage, _material, _size, _result, _description, _health, _verticalMoveSpeed, _horizontalMoveSpeed, _maxVelocity, _facingRight, _blindsight)
        {
            SetControllerTriggers();
        }

		public Player(bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_isSolid, _position, defaultImage, _material, _size, _description)
        {
            SetControllerTriggers();
        }

		public Player(Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_position, defaultImage, _material, _size, _description)
        {
            SetControllerTriggers();
        }

		public Player(Point _position, IUIElement defaultImage, Point _size, string _description) : base(_position, defaultImage, _size, _description)
        {
            SetControllerTriggers();
        }

		public Player(IUIElement defaultImage, Point _size, string _description) : base(defaultImage, _size, _description)
        {
            SetControllerTriggers();
        }

        private void SetControllerTriggers()
        {
            triggers.Add(new Trigger(TriggerConditions.ButtonDown(InteractionTypes.MOVE_UP), upR));
            triggers.Add(new Trigger(TriggerConditions.ButtonDown(InteractionTypes.MOVE_LEFT), leftR));
            triggers.Add(new Trigger(TriggerConditions.ButtonDown(InteractionTypes.MOVE_RIGHT), rightR));
            // When we fix button press and release jump should be changed back to pressed.
            triggers.Add(new Trigger(TriggerConditions.ButtonDown(InteractionTypes.JUMP), jumpR));
        }
        
        public override void Update()
        {
            base.Update();
        }
    }
}
