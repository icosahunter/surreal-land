﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Character : Entity
    {
        public float verticalMoveSpeed;
        public float horizontalMoveSpeed;
        public bool moveUp = false;
        public bool moveLeft = false;
        public bool moveRight = false;
        public bool blindsight = true;
        public bool canJump = true;

        public Character(Hitbox _hitbox, bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, Result result, string _description, int _health, float _verticalMoveSpeed, float _horizontalMoveSpeed, int _maxSpeed, bool _facingRight, bool _blindsight) : base(_hitbox, _isSolid, _position, defaultImage, _material, _size, _description, _health, _maxSpeed, _facingRight, _blindsight)
        {
            verticalMoveSpeed = _verticalMoveSpeed;
            horizontalMoveSpeed = _horizontalMoveSpeed;
        }

		public Character(bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_isSolid, _position, defaultImage, _material, _size, _description)
        {
            verticalMoveSpeed = horizontalMoveSpeed = 0.2f;
        }

		public Character(Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_position, defaultImage, _material, _size, _description)
        {
            verticalMoveSpeed = horizontalMoveSpeed = 0.2f;
        }

		public Character(Point _position, IUIElement defaultImage, Point _size, string _description) : base(_position, defaultImage, _size, _description)
        {
            verticalMoveSpeed = horizontalMoveSpeed = 0.2f;
        }

		public Character(IUIElement defaultImage, Point _size, string _description) : base(defaultImage, _size, _description)
        {
            verticalMoveSpeed = horizontalMoveSpeed = 0.2f;
        }

        public override void Update()
        {
            Tile current = ((Room)parent).map.GetTileFromPos(hitbox.center.X, hitbox.center.Y);
            canJump = ((Room)parent).map.GetTileFromPos(hitbox.center.X, hitbox.bottom + 5).topIsSolid; // && !(current.topIsSolid && current.bottomIsSolid && current.leftIsSolid && current.rightIsSolid);

            if (moveUp && canJump)
            {
                 YSpeed -= verticalMoveSpeed;
                if (YSpeed > maxSpeed)
                    YSpeed = maxSpeed;
                else if (YSpeed < -maxSpeed)
                    YSpeed = -maxSpeed;
                moveUp = false;
            }
            else if (moveUp && !canJump)
            {
                moveUp = false;
            }
            if (moveLeft)
            {
                XSpeed -= horizontalMoveSpeed;
                if (XSpeed < (-maxSpeed))
                    XSpeed = (-maxSpeed);
                moveLeft = false;
            }
            else if(moveRight)
            {
                XSpeed += horizontalMoveSpeed;
                if (XSpeed > maxSpeed)
                    XSpeed = maxSpeed;
                moveRight = false;
            }

            base.Update();
        }
    }
}
