﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Surreal_Game_1
{
    public class MovementAI
    {
        List<MovementAIComponent> aiComponents = new List<MovementAIComponent>();

        /// <summary>
        /// Adds a new component to the Movement AI for an NPC, handles case where components have the same priority.
        /// Won't add an identical component.
        /// </summary>
        /// <param name="component">The AI Component to add</param>
        public void AddComponent(MovementAIComponent component)
        {
            if (!aiComponents.Contains(component))
            {
                CheckPriorities(component);
                aiComponents.Add(component);
                aiComponents = aiComponents.OrderBy(o => (-o.priority)).ToList();
            }
        }

        /// <summary>
        /// Removes an AI component from the list
        /// </summary>
        /// <param name="component">The AI Component to remove</param>
        public void RemoveComponent(MovementAIComponent component)
        {
            if (aiComponents.Contains(component))
                aiComponents.Remove(component);
        }

        /// <summary>
        /// Makes sure that components do not have identical priority. Does recursive calls until the list has unique proper priorities.
        /// </summary>
        /// <param name="toCheck">The AI Component to check</param>
        private void CheckPriorities(MovementAIComponent toCheck)
        {
            foreach (MovementAIComponent component in aiComponents.ToList())
            {
                Console.WriteLine(component.test + " " + component.priority);
                if (component.priority == toCheck.priority)
                {
                    RemoveComponent(component);
                    component.priority++;
                    AddComponent(component);
                }
            }
        }

        /// <summary>
        /// Calls the determine movement of each component
        /// </summary>
        /// <param name="position">Position of NPC who owns this AI</param>
        /// <returns></returns>
        public MoveType DetermineMovement(Point position)
        {
            MoveType mt = MoveType.NONE;

            foreach(MovementAIComponent component in aiComponents)
            {
                mt = component.DetermineMovement(position);
                if (mt != MoveType.NONE)
                    return mt;
            }

            return mt;
        }
    }
}
