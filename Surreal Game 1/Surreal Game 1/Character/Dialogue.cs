﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public class DialogueNode
    {
        public string response;
        public Dictionary<string, DialogueNode> branches;
        public DialogueNode reentryNode;
        public DialogueNode exitNode;
        public DialogueNode skipNode;
    }

    public class Dialogue
    {
        public DialogueNode currentNode;

        public DialogueNode Start()
        {
            return currentNode;
        }

        public DialogueNode Next(string choice)
        {
            return currentNode = currentNode.branches[choice];
        }

        public DialogueNode Exit()
        {
            DialogueNode n = currentNode.exitNode;
            currentNode = currentNode.reentryNode;
            return n;
        }

        public DialogueNode Skip()
        {
            return currentNode = currentNode.skipNode;
        }
    }
}
