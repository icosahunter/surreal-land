﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Surreal_Game_1
{
    public abstract class MovementAIComponent
    {
        public MoveType moveType = MoveType.STOP;
        public int priority = 0;
        public String test = "blah";

        public MovementAIComponent(int _priority)
        {
            priority = _priority;
        }

        abstract public MoveType DetermineMovement(Point npcPosition);
    }

    public enum MoveType
    {
        NONE = -1,
        STOP = 0,
        LEFT = 1,
        RIGHT = 2,
        UP = 3,
        DOWN = 4,
        JUMP = 5
    }
}
