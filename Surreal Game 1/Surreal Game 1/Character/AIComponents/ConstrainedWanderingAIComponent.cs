﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Surreal_Game_1
{
    public class ConstrainedWanderingAIComponent : WanderingAIComponent
    {
        public int constrainedDist;
        public int startingHorizontalLoc;

        /// <summary>
        /// Full Control Constructor for Constrained Wandering
        /// </summary>
        /// <param name="_priority">Determines order that components of an AI are checked</param>
        /// <param name="_moveRate">How often an AI moves and how long movement tends to last</param>
        /// <param name="_constrainedDist">The x distance an NPC can move from its starting location</param>
        /// <param name="_startingHorizontalLoc">An NPC's starting horizontal location</param>
        public ConstrainedWanderingAIComponent(int _priority, MoveRate _moveRate, int _constrainedDist, int _startingHorizontalLoc) : base(_priority, _moveRate)
        {
            constrainedDist = _constrainedDist;
            startingHorizontalLoc = _startingHorizontalLoc;
        }

        /// <summary>
        /// Move Rate is set to the default of NORMAL
        /// </summary>
        /// <param name="_priority">Determines order that components of an AI are checked</param>
        /// <param name="_constrainedDist">The x distance an NPC can move from its starting location</param>
        /// <param name="_startingHorizontalLoc">An NPC's starting horizontal location</param>
        public ConstrainedWanderingAIComponent(int _priority, int _constrainedDist, int _startingHorizontalLoc) : base(_priority)
        {
            constrainedDist = _constrainedDist;
            startingHorizontalLoc = _startingHorizontalLoc;
        }

        /// <summary>
        /// Move Rate is set to NORMAL and constrained distance is set to 100 (not much)
        /// </summary>
        /// <param name="_priority">Determines order that components of an AI are checked</param>
        /// <param name="_startingHorizontalLoc">An NPC's starting horizontal location</param>
        public ConstrainedWanderingAIComponent(int _priority, int _startingHorizontalLoc) : base(_priority)
        {
            constrainedDist = 100;
        }

        /// <summary>
        /// Overrides the default of always going right
        /// </summary>
        /// <param name="npcPosition">Used to determine if going right is allowed</param>
        /// <returns></returns>
        override protected MoveType GoRight(Point npcPosition)
        {
            // If we are not out of bounds keep going
            if ((startingHorizontalLoc + constrainedDist) >= npcPosition.X)
                moveType = MoveType.RIGHT;

            // If we just stopped, don't keep stopping, gotta move somewhere...
            else if (moveType == MoveType.STOP)            
                moveType = MoveType.LEFT;

            // If we were going right and hit the edge
            else
            {
                walkCount = (int)Math.Floor(((int)moveRate * 7.5) - 75);
                FullStop();
            }
            return moveType;
        }

        /// <summary>
        /// Overrides default of always going left
        /// </summary>
        /// <param name="npcPosition">Used to determine if going left is allowed</param>
        /// <returns></returns>
        protected override MoveType GoLeft(Point npcPosition)
        {
            // If we are not out of bounds keep going
            if ((startingHorizontalLoc - constrainedDist) <= npcPosition.X)            
                moveType = MoveType.LEFT;

            // If we just stopped, don't keep stopping, gotta move somewhere...
            else if (moveType == MoveType.STOP)            
                moveType = MoveType.RIGHT;

            // If we were going right and hit the edge
            else
            {
                walkCount = (int)Math.Floor(((int)moveRate * 7.5) - 75);
                FullStop();
            }
            return moveType;
        }


        /// <summary>
        /// Overrides default behavior of just returning the current direction of movement
        /// </summary>
        /// <param name="npcPosition">Used to determine if the NPC can continue moving</param>
        /// <returns></returns>
        protected override MoveType KeepGoing(Point npcPosition)
        {
            // As long as we didn't hit the barrier keep going
            if ((startingHorizontalLoc - constrainedDist  >= npcPosition.X && !movingRight)|| (startingHorizontalLoc + constrainedDist <= npcPosition.X && movingRight))
            {
                walkCount = (int)Math.Floor(((int)moveRate * 7.5) - 75);
                FullStop();
            }
            return moveType;
        }
    }
}
