﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public class WanderingAIComponent : MovementAIComponent
    {
        private Random random = new Random();
        private MoveRate m_moveRate;
        public MoveRate moveRate
        {
            get { return m_moveRate; }
            set
            {
                m_moveRate = value;
                minWalk = ((int)moveRate) * 15;
            }
        }
        private int minWalk;
        protected int walkCount = 0;
        protected bool movingRight;

        /// <summary>
        /// Full Constructor for WanderingAI
        /// </summary>
        /// <param name="_priority">Determines order AI components are checked</param>
        /// <param name="_moveRate">Determines how quickly the NPC will move and how often it stops</param>
        public WanderingAIComponent(int _priority, MoveRate _moveRate) : base(_priority)
        {
            moveRate = _moveRate;
            minWalk = ((int)moveRate) * 15;
        }

        /// <summary>
        /// Move rate set to default of NORMAL
        /// </summary>
        /// <param name="_priority">Determines order AI components are checked</param>
        public WanderingAIComponent(int _priority) : base(_priority)
        {
            moveRate = MoveRate.NORMAL;
            minWalk = ((int)moveRate) * 15;
        }

        /// <summary>
        /// Just returns stop for move type. Can be overriden in child classes.
        /// </summary>
        /// <returns></returns>
        protected MoveType KeepStopping()
        {
            return MoveType.STOP;
        }

        /// <summary>
        /// Just returns the current movement direction. Can be overriden in child classes.
        /// </summary>
        /// <param name="npcPosition">Used in Constrained child class</param>
        /// <returns></returns>
        virtual protected MoveType KeepGoing(Point npcPosition)
        {
            return moveType;
        }

        /// <summary>
        /// Sets the move type and stops the AI
        /// </summary>
        /// <returns></returns>
        protected MoveType FullStop()
        {
            moveType = MoveType.STOP;
            return moveType;
        }

        /// <summary>
        /// Sets the move type to left and returns it. Can be overriden in child classes.
        /// </summary>
        /// <param name="npcPosition">Used in Constrained child class</param>
        /// <returns></returns>
        virtual protected MoveType GoLeft(Point npcPosition)
        {
            moveType = MoveType.LEFT;
            return moveType;
        }

        /// <summary>
        /// Sets the move type to right and returns it. Can be overriden in child classes.
        /// </summary>
        /// <param name="npcPosition">Used in Constrained child class</param>
        /// <returns></returns>
        virtual protected MoveType GoRight(Point npcPosition)
        {
            moveType = MoveType.RIGHT;
            return moveType;
        }

        /// <summary>
        /// The real meat and potatos of this class. Randomly determines how to move, with a little consistency in how often and how long it stops.
        /// Made so that children do not have to override it, they just override the above methods.
        /// </summary>
        /// <param name="npcPosition">Used in Constrained child components.</param>
        /// <returns></returns>
        override public MoveType DetermineMovement(Point npcPosition)
        {
            int moveRateInt = (int)moveRate;
            // WalkCount < 0 means that the NPC is in a required STOP state before moving again
            if (walkCount < 0)
            {
                walkCount++;
                return KeepStopping();
            }

            // A non-zero WalkCount less than their minWalk means that the NPC needs to continue moving for some number of frames
            else if (walkCount < minWalk && walkCount > 0)
            {
                walkCount++;
                return KeepGoing(npcPosition);
            }

            // The NPC has completed its minimum walk and must continually choose what to do next
            else if (walkCount >= minWalk && moveType != MoveType.STOP)
            {
                int rand = random.Next(1, 5);
                // SLOW and CRAWL stop 75% of the time, NORMAL stops 50%, and FAST and FRENZY stop 25%.
                if (((moveRateInt <= (int)MoveRate.SLOW) && rand == 1) ||
                    ((moveRateInt == (int)MoveRate.NORMAL) && rand <= 2) ||
                    ((moveRateInt >= (int)MoveRate.FAST) && rand <= 3))
                {
                    walkCount++;
                    return KeepGoing(npcPosition);
                }
                else
                {

                    walkCount = (moveRateInt * 15) - 150;
                    return FullStop();
                }
            }
            // WalkCount is 0 (or we have stopped for long enough), time to pick what to do and keep doing it every frame until a move occurs.
            else
            {
                walkCount = 0;
                int rand = random.Next(1, 10);

                // Time to move
                if (moveRateInt >= rand)
                {
                    walkCount++;
                    rand = random.Next(1, 10);

                    if (rand % 2 == 0)
                    {
                        movingRight = false;
                        return GoLeft(npcPosition);
                    }
                    else
                    {
                        movingRight = true;
                        return GoRight(npcPosition);
                    }
                }
                else
                {
                    return moveType;
                }
            }
        }
    }

    /// <summary>
    /// A handy little enum for setting move rate that also has integers useful to the math of determining how often to move.
    /// </summary>
    public enum MoveRate
    {
        NONE = -1,
        CRAWL = 1,
        SLOW = 3,
        NORMAL = 5,
        FAST = 7,
        FRENZY = 9
    }
}
