﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public class RunAwayAIComponent : MovementAIComponent
    {
        public Element toConsider = null;
        public int healthyDistance;

        /// <summary>
        /// The full constructor
        /// </summary>
        /// <param name="_priority">Determines order that AI Components are checked</param>
        /// <param name="_toConsider">The element to run away from</param>
        /// <param name="_distance">The minimum distance to keep away</param>
        public RunAwayAIComponent(int _priority, Element _toConsider, int _distance) : base(_priority)
        {
            toConsider = _toConsider;
            healthyDistance = _distance;
        }

        /// <summary>
        /// Distance is set to 100, this AI is not too afraid, that's close...
        /// </summary>
        /// <param name="_priority">Determines order that AI Components are checked</param>
        /// <param name="_toConsider">The element to run away from</param>
        public RunAwayAIComponent(int _priority, Element _toConsider) : base(_priority)
        {
            toConsider = _toConsider;
            healthyDistance = 100;
        }

        /// <summary>
        /// The meat and potatos of the AI, determines where to go
        /// </summary>
        /// <param name="npcPosition">Position of the NPC that owns this AI</param>
        /// <returns></returns>
        override public MoveType DetermineMovement(Point npcPosition)
        {
            MoveType moveType = MoveType.NONE;
            if ((toConsider is Entity && !((Entity)toConsider).isDestroyed) || toConsider is Element)
            {
                if (Math.Abs(toConsider.position.X - npcPosition.X) < healthyDistance && (toConsider.position.X - npcPosition.X) > 0)
                {
                    moveType = MoveType.LEFT;
                }
                else if (Math.Abs(toConsider.position.X - npcPosition.X) < healthyDistance && (toConsider.position.X - npcPosition.X) < 0)
                {
                    moveType = MoveType.RIGHT;
                }
            }
            return moveType;
        }
    }
}
