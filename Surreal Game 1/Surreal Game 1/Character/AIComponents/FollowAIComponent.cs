﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public class FollowAIComponent : MovementAIComponent
    {
        public Element toConsider = null;
        public int healthyDistance;

        /// <summary>
        /// Full constructor
        /// </summary>
        /// <param name="_priority">Determines order that AI Components are checked</param>
        /// <param name="_toConsider">The Element this AI will follow</param>
        /// <param name="_distance">How close an AI must get before stopping</param>
        public FollowAIComponent(int _priority, Element _toConsider, int _distance) : base(_priority)
        {
            toConsider = _toConsider;
            healthyDistance = _distance;
        }

        /// <summary>
        /// Distance is set to a default of 100, a pretty close follow
        /// </summary>
        /// <param name="_priority">Determines order that AI Components are checked</param>
        /// <param name="_toConsider">The Element this AI will follow</param>
        public FollowAIComponent(int _priority, Element _toConsider) : base(_priority)
        {
            toConsider = _toConsider;
            healthyDistance = 100;
        }

        /// <summary>
        /// The meat and potatos, decides where to move to properly follow.
        /// </summary>
        /// <param name="npcPosition">The position of the NPC this AI belongs to</param>
        /// <returns></returns>
        override public MoveType DetermineMovement(Point npcPosition)
        {
            MoveType moveType = MoveType.NONE;
            if ((toConsider is Entity && !((Entity)toConsider).isDestroyed) || toConsider is Element)
            {
                if ((toConsider.position.X - toConsider.size.X - healthyDistance) > npcPosition.X)
                {
                    moveType = MoveType.RIGHT;
                }
                else if ((toConsider.position.X + toConsider.size.X + healthyDistance) < npcPosition.X)
                {
                    moveType = MoveType.LEFT;
                }
            }
            return moveType;
        }
    }
}
