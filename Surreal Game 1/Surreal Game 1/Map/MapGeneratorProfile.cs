﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    class MapGeneratorProfileEntry
    {
        public class ArrayAndOffset
        {
            public Point offset;
            public int[,] indexArray;

            public ArrayAndOffset(int[,] array, Point offsetPos)
            {
                offset = offsetPos;
                indexArray = array;
            }
        }

        public int[,] target;
        WeightedSelector<ArrayAndOffset> generationProfile = new WeightedSelector<ArrayAndOffset>();

        public MapGeneratorProfileEntry(int targetTileIndex)
        {
            target = new int[,] {{targetTileIndex}};
        }
        
        public MapGeneratorProfileEntry(int[,] targetTileArray)
        {
            target = targetTileArray;
        }

        /// <summary>
        /// Add the specified tileIndex, topGenChance, rightGenChance, bottomGenChance and leftGenChance.
        /// </summary>
        /// <param name="tileIndex">Index of the tile to describe generation for</param>
        /// <param name="topGenWeight">Chance of generating a tile with index <paramref name="tileIndex"/> on top of the tile this profile is for</param>
        /// <param    name="rightGenWeight">Chance of generating a tile with index <paramref name="tileIndex"/> on the right of the tile this profile is for</param>
        /// <param name="bottomGenWeight">Chance of generating a tile with index <paramref name="tileIndex"/> on the bottom of the tile this profile is for</param>
        /// <param name="leftGenWeight">Chance of generating a tile with index <paramref name="tileIndex"/> on the left of the tile this profile is for</param>
        public void Add(int tileIndex, int topGenWeight, int rightGenWeight, int bottomGenWeight, int leftGenWeight)
        {
            ArrayAndOffset topGen = new ArrayAndOffset(new int[,] { { tileIndex } }, new Point(0, -1));
            ArrayAndOffset rightGen = new ArrayAndOffset(new int[,] { { tileIndex } }, new Point(1, 0));
            ArrayAndOffset bottomGen = new ArrayAndOffset(new int[,] { { tileIndex } }, new Point(0, 1));
            ArrayAndOffset leftGen = new ArrayAndOffset(new int[,] { { tileIndex } }, new Point(-1, 0));
            generationProfile.Add(topGen, topGenWeight);
            generationProfile.Add(rightGen, rightGenWeight);
            generationProfile.Add(bottomGen, bottomGenWeight);
            generationProfile.Add(leftGen, leftGenWeight);
        }
        public void Add(int[,] genPattern, Point genOffset, int genWeight, bool horizontalSymmetry = false, bool verticalSymmetry = false)
        {
            ArrayAndOffset patternAndOffset = new ArrayAndOffset(genPattern, genOffset);
            generationProfile.Add(patternAndOffset, genWeight);
        }

        public ArrayAndOffset GetGenPattern(double randomNum)
        {
            return generationProfile.Select(randomNum);
        }
        
        public ArrayAndOffset GetGenPattern()
        {
            return generationProfile.Select();
        }
    }
    class MapGeneratorProfile
    {
        public List<MapGeneratorProfileEntry> entries = new List<MapGeneratorProfileEntry>();
        public List<Point> targetSizes = new List<Point>();

        public void Add(MapGeneratorProfileEntry entry)
        {
            Point size = new Point(entry.target.GetLength(1), entry.target.GetLength(0));

            if (targetSizes.Where(x => x.X == size.X && x.Y == size.Y).Count() < 1)
            {
                targetSizes.Add(size);
            }

            entries.Add(entry);
        }

        public void AddRange(List<MapGeneratorProfileEntry> entries)
        {
            foreach(MapGeneratorProfileEntry entry in entries)
            {
                Add(entry);
            }
        }
        
        public MapGeneratorProfileEntry Search(int[,] target)
        {
            foreach (MapGeneratorProfileEntry entry in entries.Where(x => x.target.Length == target.Length))
            {
                int[] newTar = target.Cast<int>().Select((a, i) => a == -1 ? entry.target.Cast<int>().ToArray()[i] : a).ToArray();
                int[] newEnt = entry.target.Cast<int>().Select((a, i) => a == -1 ? newTar[i] : a).ToArray();
                if (newEnt.SequenceEqual(newTar))
                {
                    return entry;
                }
            }
            return null;

            //return entries.Find( x => 

            //    x.target.Length == target.Length && //if the entry's target length is equal to the target length
                                    
            //    x.target.Cast<int>() //cast the entry's target to a 1D array

            //    .SequenceEqual //check if this is equal to the target except where the target has -1s, those are ok
            //    (
            //        target.Cast<int>()
            //        .Select
            //        (
            //            (a, i) => a == -1 ? x.target.Cast<int>().ToArray()[i] : a
            //        )
            //        .ToArray()
            //    )
            //);
        }
    }
}
