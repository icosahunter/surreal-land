﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public enum Neighborhood { VON_NEUMANN, MOORE };
    static class TerrainGenerator
    {
        public static int[,] Layers(Point size, int[] tileIndexes, int[] layerHeights, double roughness = 0.2, double flatness = 3)
        {
            double[,] temp = SignalProcessing.VerticalGradient(0, 1, size.Y, size.X);
            temp = temp.MultiplyByColumns(SignalProcessing.Resize(SignalProcessing.RandomNoise((int)(size.X / flatness)).Multiply(roughness), size.X).Add(1));
            double[] stops = new double[tileIndexes.Length];
            double[] lh = SignalProcessing.Normalize(SignalProcessing.CumulitiveSum(layerHeights.ConvertToDouble()));
            int[,] tileArray = SignalProcessing.Discretize(temp, lh);

            Dictionary<int, int> swapDict = new Dictionary<int, int>();
            for (int i = 0; i < tileIndexes.Length; i++)
            {
                swapDict.Add(i, tileIndexes[i]);
            }

            tileArray = SwapTiles(tileArray, swapDict);

            return tileArray;
        }

        public static int[,] Caverns(Point size, int[] tileIndexes, int[] layerHeights, bool includeBorder = true, double vertDensity = 0.7, double horDensity = 0.7, int smoothness = 4)
        {
            double[,] temp = SignalProcessing.RandomNoise((int)(size.Y * vertDensity), (int)(size.X * horDensity));
            temp = SignalProcessing.Resize(temp, size.Y, size.X);
            temp = SignalProcessing.MedianSmooth(temp, 5, smoothness, false, SignalProcessing.BilinearWindow);
            temp = SignalProcessing.MedianSmooth(temp, 5, 1, false);

            double[] lh = SignalProcessing.Normalize(SignalProcessing.CumulitiveSum(layerHeights.ConvertToDouble()));
            int[,] tileArray = SignalProcessing.Discretize(temp, lh);

            Dictionary<int, int> swapDict = new Dictionary<int, int>();
            for (int i = 0; i < tileIndexes.Length; i++)
            {
                swapDict.Add(i, tileIndexes[i]);
            }

            tileArray = SwapTiles(tileArray, swapDict);

            if(includeBorder)
            {
                tileArray = Composite(NaturalBorder(size, tileIndexes, layerHeights), tileArray);
            }

            return tileArray;
        }

        public static int[,] SwapTiles(int[,] tileArray, Dictionary<int, int> indexesAndSwapIndexes)
        {
            int h = tileArray.GetLength(0);
            int w = tileArray.GetLength(1);
            int[,] ar = new int[h, w];
            for(int i = 0; i < h; i++)
            {
                for(int j = 0; j < w; j++)
                {
                    if (indexesAndSwapIndexes.ContainsKey(tileArray[i, j]))
                    {
                        ar[i, j] = indexesAndSwapIndexes[tileArray[i, j]];
                    }
                }
            }
            return ar;
        }

        static public int[,] Border(Point size, int tileIndex, bool[] sides)
        {
            int[,] ar = new int[size.Y, size.X];

            if (sides[0] || sides[2])
            {
                for (int i = 0; i < size.X; i++)
                {
                    if (sides[0])
                    {
                        ar[0, i] = tileIndex;
                    }
                    if (sides[2])
                    {
                        ar[size.Y - 1, i] = tileIndex;
                    }
                }
            }

            if (sides[1] || sides[3])
            {
                for (int i = 0; i < size.Y; i++)
                {
                    if (sides[1])
                    {
                        ar[i, size.X - 1] = tileIndex;
                    }
                    if (sides[3])
                    {
                        ar[i, 0] = tileIndex;
                    }
                }
            }

            return ar;
        }

        static public int[,] NaturalBorder(Point size, int[] tileIndexes, int[] layerHeights, double roughness = 0.15, double flatness = 3)
        {
            double[,] temp = SignalProcessing.RectangularGradient(1, 0, size.Y, size.X);
            temp = temp.MultiplyByColumns(SignalProcessing.Resize(SignalProcessing.RandomNoise((int)(size.X / flatness)).Multiply(roughness), size.X).Add(1));
            temp = temp.MultiplyByRows(SignalProcessing.Resize(SignalProcessing.RandomNoise((int)(size.X / flatness)).Multiply(roughness), size.X).Add(1));
            temp = SignalProcessing.NatLog(temp.Multiply(1.3));

            double[] lh = SignalProcessing.Normalize(SignalProcessing.CumulitiveSum(layerHeights.ConvertToDouble()));
            int[,] tileArray = SignalProcessing.Discretize(temp, lh);
            
            Dictionary<int, int> swapDict = new Dictionary<int, int>();
            for (int i = 0; i < tileIndexes.Length; i++)
            {
                swapDict.Add(i, tileIndexes[i]);
            }

            tileArray = SwapTiles(tileArray, swapDict);

            return tileArray;
        }

        public static int[,] BitMask(int[,] map, int[] mask, Neighborhood neighborhood = Neighborhood.VON_NEUMANN)
        {
            switch(neighborhood)
            {
                case Neighborhood.VON_NEUMANN: return VonNeumannBitMask(map, mask);
                case Neighborhood.MOORE: throw new NotImplementedException();
                default: throw new Exception();
            }
        }

        private static int[,] VonNeumannBitMask(int[,] map, int[] mask)
        {
            int[,] newMap = new int[map.GetLength(0), map.GetLength(1)];
            for (int r = 0; r < map.GetLength(0); r++)
            {
                for (int c = 0; c < map.GetLength(1); c++)
                {
                    if (map[r, c] > 0)
                    {
                        int val = 0;
                        try { val += 1 * map[r - 1, c].OneIfOverZero(); }
                        catch { val += 1; }

                        try { val += 2 * map[r, c + 1].OneIfOverZero(); }
                        catch { val += 2; }

                        try { val += 4 * map[r + 1, c].OneIfOverZero(); }
                        catch { val += 4; }

                        try { val += 8 * map[r, c - 1].OneIfOverZero(); }
                        catch { val += 8; }
                        newMap[r, c] = mask[val];
                    }
                    else
                    {
                        newMap[r, c] = 0;
                    }
                }
            }
            return newMap;
        }

        public static int[,] Composite(int[,] tileArray1, int[,] tileArray2)
        {
            int rs = tileArray1.GetLength(0);
            int cs = tileArray1.GetLength(1);
            int[,] ar = (int[,])tileArray1.Clone();
            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    if (tileArray2[i, j] != 0)
                    {
                        ar[i, j] = tileArray2[i, j];
                    }
                }
            }
            return ar;
        }

        static public void AddBlock(int[,] array, Point position, int[,] block)
        {
            for (int x = 0; x < block.GetLength(1); x++)
            {
                for (int y = 0; y < block.GetLength(0); y++)
                {
                    try
                    {
                        array[position.Y + y, position.X + x] = block[y, x];
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
        }
    }
}
