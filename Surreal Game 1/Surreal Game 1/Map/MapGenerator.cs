﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using System.IO;

namespace Surreal_Game_1
{
    static class MapGenerator
    {
        public class VisGenMap
        {
            public Random random = new Random();
            public Map map;
            public MapGeneratorProfile profile;
            public int frameSkip;
            public int step = 0;
            public int frame = 0;

            public VisGenMap(Map map_, MapGeneratorProfile profile_, int delay_)
            {
                map = map_;
                profile = profile_;
                frameSkip = delay_;
            }

            public void NextGenStep()
            {
                if (frame % frameSkip == 0)
                {
                    if (step >= map.tileArray.Length)
                    {
                        step = 0;
                    }
                    int x = step % map.tileArray.GetLength(1);
                    int y = step / map.tileArray.GetLength(1);
                    //int x = random.Next(map.tileArray.GetLength(1));
                    //int y = random.Next(map.tileArray.GetLength(0));
                    foreach (Point size in profile.targetSizes)
                    {
                        int[,] target = map.tileArray.SubArray(x, y, x + size.X, y + size.Y);
                        MapGeneratorProfileEntry thisTileEntry = profile.Search(target);

                        if (thisTileEntry != null)
                        {
                            double r = random.NextDouble();
                            MapGeneratorProfileEntry.ArrayAndOffset AandO = thisTileEntry.GetGenPattern(r);

                            if (AandO != null)
                            {
                                Point pos = new Point(x + AandO.offset.X, y + AandO.offset.Y);
                                AddBlock(map, pos, AandO.indexArray);
                            }
                        }
                    }
                    step++;
                }
                frame++;
            }
        }
        public class ArrayAndPosition
        {
            public int[,] indexArray;
            public Point position;
            
            public ArrayAndPosition(int[,] pattern, Point pos)
            {
                indexArray = pattern;
                position = pos;
            }
        }
        static public Map Pepper(Map seed, int tileIndexToPepper, double density)
        {
            Random random = new Random();
            int[,] toPepper = new int[,] { { tileIndexToPepper } };
            Map newMap = new Map(seed.tileArray, seed.tileTypes, seed.tileSize);
            for (int x = 0; x < newMap.tileArray.GetLength(1); x++)
            {
                for (int y = 0; y < newMap.tileArray.GetLength(0); y++)
                {               
                    double r = random.NextDouble();
                    if (r > 0 && r < density)
                    {
                        AddBlock(newMap, new Point(x, y), toPepper);
                    }
                }
            }

            return newMap;
        }

        static public Map Generate(Map seed, MapGeneratorProfile profile, int genPasses)
        {
            Random random = new Random();
            Map newMap = new Map(seed.tileArray, seed.tileTypes, seed.tileSize);

            for (int i = 0; i < genPasses; i++)
            {
                for(int x = 0; x < newMap.tileArray.GetLength(1); x++)
                {
                    for(int y = 0; y < newMap.tileArray.GetLength(0); y++)
                    {
                        foreach (Point size in profile.targetSizes)
                        {
                            int[,] target = newMap.tileArray.SubArray(x, y, x + size.X, y + size.Y);
                            MapGeneratorProfileEntry thisTileEntry = profile.Search(target);

                            if (thisTileEntry != null)
                            {
                                double r = random.NextDouble();
                                MapGeneratorProfileEntry.ArrayAndOffset AandO = thisTileEntry.GetGenPattern(r);

                                if (AandO != null)
                                {
                                    Point pos = new Point(x + AandO.offset.X, y + AandO.offset.Y);
                                    AddBlock(newMap, pos, AandO.indexArray);
                                }
                            }
                        }
                    }
                }
            }
            return newMap;
        }

        static public int[,] GenSeed(Point size, int bedrock, bool top = false, bool right = false, bool bottom = true, bool left = false)
        {
            int[,] ar = new int[size.Y, size.X];

            if (top || bottom)
            {
                for (int i = 0; i < size.X; i++)
                {
                    if (top)
                    {
                        ar[0, i] = bedrock;
                    }
                    if (bottom)
                    {
                        ar[size.Y - 1, i] = bedrock;
                    }
                }
            }

            if (right || left)
            {
                for (int i = 0; i < size.Y; i++)
                {
                    if (right)
                    {
                        ar[i, size.X - 1] = bedrock;
                    }
                    if (left)
                    {
                        ar[i, 0] = bedrock;
                    }
                }
            }

            return ar;
        }

        static public void AddBlock(Map map, Point place, int[,] array)
        {
            for (int x = 0; x < array.GetLength(1); x++)
            {
                for (int y = 0; y < array.GetLength(0); y++)
                {
                    try
                    {
                        map.tileArray[place.Y + y, place.X + x] = array[y, x];
                    }
                    catch(Exception e)
                    {

                    }
                }
            }
        }
    }
}
