﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Map : UIElement, ICanHaveCamera
    {
        public List<Tile> tileTypes { get; protected set; }
        public int[,] tileArray;
        public Camera camera { get; set; }
        public bool cameraMode { get; set; }
        public Point tileSize;

        public Map(int[,] _tileArray, List<Tile> _tileTypes, Point _tileSize)
        {
            tileArray = _tileArray;
            cameraMode = true;
            foreach (Tile t in _tileTypes)
            {
                t.camera = camera;
                t.parent = this;
            }
            tileTypes = _tileTypes;
            tileSize = _tileSize;
            position = new Point(0, 0);
        }

        public override void Draw(SpriteBatch batch, Point pos, Point drawsize)
        {
            base.Draw(batch, pos, drawsize);
            for (int i = 0; i < tileArray.GetLength(1); i++)
            {
                for (int j = 0; j < tileArray.GetLength(0); j++)
                {
                    Point p = new Point(pos.X + (i * drawsize.X / tileArray.GetLength(1)), pos.Y + (j * drawsize.Y / tileArray.GetLength(0)));
                    Point s = new Point(drawsize.X / tileArray.GetLength(1), drawsize.Y / tileArray.GetLength(0));

                    if (tileArray[j, i] > 0)
                    {
                        tileTypes[tileArray[j, i] - 1].Draw(batch, p, s);
                    }

                    if (tileArray[j, i] < 0)
                    {
                        DefaultContent.XTile.Draw(batch, p, s);
                    }
                }
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            for (int i = 0; i < tileArray.GetLength(1); i++)
            {
                for (int j = 0; j < tileArray.GetLength(0); j++)
                {
                    Point pos = new Point(position.X + (i * tileSize.X), position.Y + (j * tileSize.Y));

                    if (tileArray[j, i] > 0)
                    {
                        tileTypes[tileArray[j, i] - 1].Draw(batch, pos, tileSize);
                    }

                    if (tileArray[j, i] < 0)
                    {
                        DefaultContent.XTile.Draw(batch, pos, tileSize);
                    }
                }
            }
        }

        public void AddTileType(Tile t)
        {
            t.camera = camera;
            t.parent = this;
            tileTypes.Add(t);
        }

        public void RemoveTileType(Tile t)
        {
            tileTypes.Remove(t);
            t.parent = null;
        }

        public Tile GetTile(int X, int Y)
        {
            if (X < 0 || Y < 0 || X >= tileArray.GetLength(1) || Y >= tileArray.GetLength(0))
            {
                return Tile.Default;
            }
            else if (tileArray[Y, X] < 0)
            {
                return DefaultContent.XTile;
            }
            else if (tileArray[Y, X] == 0)
            {
                return Tile.Default;
            }
            else
            {
                return tileTypes[tileArray[Y, X] - 1];
            }
        }

        public Tile GetTile(Point point)
        {
            if (point.X < 0 || point.Y < 0 || point.X >= tileArray.GetLength(1) || point.Y >= tileArray.GetLength(0))
            {
                return Tile.Default;
            }
            else if (tileArray[point.Y, point.X] < 0)
            {
                return DefaultContent.XTile;
            }
            else if (tileArray[point.Y, point.X] == 0)
            {
                return Tile.Default;
            }
            else
            {
                return tileTypes[tileArray[point.Y, point.X] - 1];
            }
        }

        public Tile GetTileFromPos(int X, int Y)
        {
            return GetTile(X / tileSize.X, Y / tileSize.Y);
        }

        public Tile GetTileFromPos(Point point)
        {
            return GetTile(point.X / tileSize.X, point.Y / tileSize.Y);
        }

        //TODO: Add logic to make sure the tile actually exists before returning its location.
        public Rectangle GetTileBounds(int X, int Y)
        {
            return new Rectangle(new Point(position.X + (X * tileSize.X), position.Y + (Y * tileSize.Y)), tileSize);
        }
        
        public Hitbox GetTileHitbox(int X, int Y)
        {
            Rectangle r = GetTileBounds(X, Y);
            return new RectHitbox(r, Math.Min(r.Height, r.Width) / 3);
        }
    }
}
