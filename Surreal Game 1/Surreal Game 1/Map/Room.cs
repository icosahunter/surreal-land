﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Room : UIElement
    {
        public Map map;
        public Texture2D background;
        public Rectangle backgroundPos;
        public Camera camera { get; set; }
        public string name;
        public GameScreen screen;

        public Room(GameScreen screen_, Map _map, Texture2D _background, string _name, bool keepBackgroundRatio = false)
        {
            screen = screen_;
            map = _map;
            background = _background;
            if (keepBackgroundRatio)
            {
                if (background.Width/background.Height >= screen.window.width/screen.window.height)
                {
                    backgroundPos = new Rectangle(0, 0, screen.window.graphics.PreferredBackBufferWidth, screen.window.width * background.Height / background.Width);
                }
                else
                {
                    backgroundPos = new Rectangle(0, 0, screen.window.height * background.Width / background.Height, screen.window.height);
                }
            }
            else
            {
                backgroundPos = new Rectangle(0, 0, screen.window.width, screen.window.height);
            }
            name = _name;
            camera = screen.camera;
            map.camera = camera;
        }

        public Room(GameScreen screen_, Map _map, Texture2D _background, Rectangle _backgroundPos, string _name)
        {
            screen = screen_;
            map = _map;
            background = _background;
            backgroundPos = _backgroundPos;
            name = _name;
            camera = screen.camera;
            map.camera = camera;
        }

        public Room(GameScreen screen_, Map _map, string _name)
        {
            screen = screen_;
            map = _map;
            background = null;
            backgroundPos = new Rectangle(0,0,0,0);
            name = _name;
            camera = screen.camera;
            map.camera = camera;
        }

        public void AddGameElement(IGameElement element)
        {
            element.camera = camera;
            base.AddChild(element);
        }

        public void AddElements(List<IGameElement> _elements)
        {
            foreach (IGameElement e in _elements)
            {
                e.camera = camera;
                base.AddChild(e);
            }
        }

        public override void Update()
        {
            foreach (IGameElement e1 in children)
            {
                if (e1 is Entity)
                {
                    Environment.GravityCheck((Entity)e1, map);
                    //((Entity)e1).canJump = map.GetTileFromPos(e1.center.X, ((Entity)e1).hitbox.bottom).topIsSolid;

                    // This is the unholy terror, approach at your own risk
                    // Switch all of these to hitbox.Bottom.X and hitbox.Bottom.Y
                    //if (((Entity)e1).flying)
                    //    e1.checkPosition(map.GetNextVerticalTileFromPosition(e1.center.X, e1.center.Y, ((Entity)e1).flying, ((Entity)e1).falling), TileLoc.ABOVE);
                    //else if (((Entity)e1).falling)
                    //    e1.checkPosition(map.GetNextVerticalTileFromPosition(e1.center.X, e1.center.Y, ((Entity)e1).flying, ((Entity)e1).falling), TileLoc.BELOW);
                    //else
                    //{
                    //    e1.checkPosition(map.GetNextHorizontalTileFromPosition(e1.center.X, e1.center.Y, ((Entity)e1).facingRight), TileLoc.LEVEL);
                    //    e1.checkPosition(map.GetNextHorizontalTileBelowFromPosition(e1.center.X, e1.center.Y, ((Entity)e1).facingRight), TileLoc.BELOW);
                    //}

                }

                foreach (IGameElement e2 in children)
                {
                    Environment.Physics(e1, e2);
                    foreach (Trigger t in e1.triggers)
                    {
                        t.Update(e1, e2);
                    }
                }

                foreach(Trigger t in e1.triggers)
                {
                    t.Update(e1, this);
                }
            }

            base.Update();
        }

        public override void Draw(SpriteBatch batch)
        {
            if (background != null)
            {
                batch.Draw(background, backgroundPos, Color.White);
            }

            map.Draw(batch);

            base.Draw(batch);
        }

        public override void Draw(SpriteBatch batch, Point pos, Point drawsize)
        {
            if (background != null)
            {
                batch.Draw(background, backgroundPos, Color.White);
            }

            map.Draw(batch);

            base.Draw(batch, pos, drawsize);
        }

        public void Navigate(Room navigateTo)
        {
            screen.currentRoom = navigateTo;
        }
    }
}
