﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public enum TileType
    {
        ISOLATED,
        SOUTH_CAP, WEST_CAP, NORTH_CAP, EAST_CAP,
        SOUTHWEST_CORNER, SOUTHEAST_CORNER, NORTHWEST_CORNER, NORTHEAST_CORNER,
        VERTICAL_BEAM, HORIZONTAL_BEAM,
        SOUTH_FACE, NORTH_FACE, EAST_FACE, WEST_FACE,
        FILL,
        SOUTHWEST_DIAGONAL, SOUTHEAST_DIAGONAL, NORTHWEST_DIAGONAL, NORTHEAST_DIAGONAL
    }

    public class Tile : GameElement
    {
        public static Tile Default = new Tile(null);
        public override bool cameraMode { get { return ((Map)parent).cameraMode; } set { } }
        public override Camera camera { get { return ((Map)parent).camera; } set { } }

        public Tile(IUIElement _texture)
        {
            material = Material.Default;
            image = _texture;
            topIsSolid = false;
            rightIsSolid = false;
            bottomIsSolid = false;
            leftIsSolid = false;
            autoFormatting = true;
        }

        public Tile(IUIElement _texture, bool[] solidSides)
        {
            material = Material.Default;
            image = _texture;
            topIsSolid = solidSides[0];
            rightIsSolid = solidSides[1];
            bottomIsSolid = solidSides[2];
            leftIsSolid = solidSides[3];
            autoFormatting = true;
        }

        public Tile(IUIElement _texture, Material _material)
        {
            material = _material;
            image = _texture;
            topIsSolid = false;
            rightIsSolid = false;
            bottomIsSolid = false;
            leftIsSolid = false;
            autoFormatting = true;
        }

        public Tile(IUIElement _texture, Material _material, bool[] solidSides)
        {
            material = _material;
            image = _texture;
            topIsSolid = solidSides[0];
            rightIsSolid = solidSides[1];
            bottomIsSolid = solidSides[2];
            leftIsSolid = solidSides[3];
            autoFormatting = true;
        }
    }
}
