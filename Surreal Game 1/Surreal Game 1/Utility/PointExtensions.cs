﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public static class PointExtensions
    {
        public static Point MultipliedBy(this Point p, int n)
        {
            return new Point(n * p.X, n * p.Y);
        }

        public static Point DividedBy(this Point p, int n)
        {
            return new Point(p.X / n, p.Y / n);
        }

        public static Point MultipliedBy(this Point p, double d)
        {
            return new Point((int)(d * (double)p.X), (int)(d * (double)p.Y));
        }

        public static Point DividedBy(this Point p, double d)
        {
            return new Point((int)((double)p.X / d), (int)((double)p.Y / d));
        }

        public static Point MultipliedBy(this Point p, Vector2 v)
        {
            return new Point((int)(v.X * (double)p.X), (int)(v.Y * (double)p.Y));
        }

        public static Point DividedBy(this Point p, Vector2 v)
        {
            return new Point((int)((double)p.X / v.X), (int)((double)p.Y / v.Y));
        }

        public static Point Add(this Point p, Vector2 v)
        {
            return new Point((int)(v.X + (double)p.X), (int)(v.Y + (double)p.Y));
        }

        public static Point Subtract(this Point p, Vector2 v)
        {
            return new Point((int)((double)p.X - v.X), (int)((double)p.Y - v.Y));
        }

        public static double DistanceTo(this Point p, Point p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p.X, 2) + Math.Pow(p2.Y - p.Y, 2));
        }

        public static Vector2 ToVector2(this Point p) { return new Vector2(p.X, p.Y); }
    }
}
