﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class WeightedSelector<T>
    {
		struct WeightedEntry
        {
            public T item;
            public int weight;
		}  

		Random random = new Random();
		List<WeightedEntry> entries = new List<WeightedEntry>();

        public void Add(T item, int weight)
		{
			WeightedEntry entry = new WeightedEntry();
			entry.item = item;
			if (entries.Count == 0)
			{
				entry.weight = weight;
			}
			else
			{
				entry.weight = entries.Last().weight + weight;
			}
  			entries.Add(entry);
		}
        
		//public void Remove(T item, int weight)
		//{
		//	WeightedEntry entry = new WeightedEntry();
        //    entry.item = item;
        //    entry.weight = weight;
		//	entries.Remove(entry);
		//}
        
        public T Select(double randomDouble)
		{
			int selectNum = (int)(randomDouble * entries.Last().weight);
			return entries.Find(x => x.weight > selectNum).item;
		}

        public T Select()
		{
			return Select(random.NextDouble());
		}
    }
}
