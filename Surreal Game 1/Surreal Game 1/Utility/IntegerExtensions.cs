﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public static class IntegerExtensions
    {
        public static int OneIfOverZero(this int i)
        {
            return i > 0 ? 1 : 0;
        }
    }
}
