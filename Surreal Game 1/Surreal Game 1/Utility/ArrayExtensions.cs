﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Surreal_Game_1
{
    public static class ArrayExtensions
    {
        public static bool Equals<T>(this T[,] array1, T[,] array2)
        {
			return array1.Cast<T>().SequenceEqual(array2.Cast<T>());
        }

		public static bool Equals<T>(this T[,,] array1, T[,,] array2)
        {
            return array1.Cast<T>().SequenceEqual(array2.Cast<T>());
        }

		public static bool Equals<T>(this T[,,,] array1, T[,,,] array2)
        {
            return array1.Cast<T>().SequenceEqual(array2.Cast<T>());
        }
        
		public static bool Equals<T>(this T[][] array1, T[][] array2)
        {
            return array1.Cast<T>().SequenceEqual(array2.Cast<T>());
        }

        public static bool Equals<T>(this T[][][] array1, T[][][] array2)
        {
            return array1.Cast<T>().SequenceEqual(array2.Cast<T>());
        }

        public static bool Equals<T>(this T[][][][] array1, T[][][][] array2)
        {
            return array1.Cast<T>().SequenceEqual(array2.Cast<T>());
        }

        public static int[] IndexOfAll<T>(this T[,] array, Predicate<T> predicate)
        {
            return array.Cast<T>().Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static int[] IndexOfAll<T>(this T[,,] array, Predicate<T> predicate)
        {
            return array.Cast<T>().Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static int[] IndexOfAll<T>(this T[,,,] array, Predicate<T> predicate)
        {
            return array.Cast<T>().Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static int[] IndexOfAll<T>(this T[] array, Predicate<T> predicate)
        {
            return array.Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static int[] IndexOfAll<T>(this T[][] array, Predicate<T> predicate)
        {
            return array.Cast<T>().Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static int[] IndexOfAll<T>(this T[][][] array, Predicate<T> predicate)
        {
            return array.Cast<T>().Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static int[] IndexOfAll<T>(this T[][][][] array, Predicate<T> predicate)
        {
            return array.Cast<T>().Select((x, i) => predicate(x) ? i : -1).Where(x => x != -1).ToArray();
        }

        public static double[] Add(this double[] ar1, double n)
        {
            return ar1.Select(x => x + n).ToArray();
        }

        public static double[] Add(this double[] ar1, double[] ar2)
        {
            return ar1.Select((x, i) => x + (i < ar2.Length ? ar2[i] : 1)).ToArray();
        }

        public static double[,] Add(this double[,] ar1, double n)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    ar[i, j] = ar1[i, j] + n;
                }
            }
            return ar;
        }

        public static double[,] AddByRows(this double[,] ar1, double[] ar2)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    try
                    {
                        ar[i, j] = ar1[i, j] + ar2[i];
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                }
            }
            return ar;
        }

        public static double[,] AddByColumns(this double[,] ar1, double[] ar2)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    try
                    {
                        ar[i, j] = ar1[i, j] + ar2[j];
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                }
            }
            return ar;
        }

        public static double[,] Add(this double[,] ar1, double[,] ar2)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    try
                    {
                        ar[i, j] = ar1[i, j] + ar2[i, j];
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                }

            }
            return ar;
        }

        public static double[] Multiply(this double[] ar1, double[] ar2)
        {
            return ar1.Select((x, i) => x * (i < ar2.Length ? ar2[i] : 1)).ToArray();
        }

        public static double[] Multiply(this double[] ar1, double n)
        {
            return ar1.Select(x => x * n).ToArray();
        }

        public static double[,] Multiply(this double[,] ar1, double n)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    ar[i, j] = ar1[i, j] * n;
                }
            }
            return ar;
        }

        public static double[,] MultiplyByRows(this double[,] ar1, double[] ar2)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    try
                    {
                        ar[i, j] = ar1[i, j] * ar2[i];
                    }
                    catch(IndexOutOfRangeException)
                    {

                    }
                }
            }
            return ar;
        }

        public static double[,] MultiplyByColumns(this double[,] ar1, double[] ar2)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < C; j++)
                {
                    try
                    {
                        ar[i, j] = ar1[i, j] * ar2[j];
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                }
            }
            return ar;
        }

        public static double[,] Multiply(this double[,] ar1, double[,] ar2)
        {
            int R = ar1.GetLength(0);
            int C = ar1.GetLength(1);
            double[,] ar = new double[R, C];
            for(int i = 0; i < R; i++)
            {
                for(int j = 0; j < C; j++)
                {
                    try
                    {
                        ar[i, j] = ar1[i, j] * ar2[i, j];
                    }
                    catch (IndexOutOfRangeException)
                    {

                    }
                }

            }
            return ar;
        }

        public static T[] SubArray<T>(this T[] array, int i1, int i2)
        {
            return array.Where((x, i) => i >= i1 && i < i2).ToArray();
        }

        public static T[,] SubArray<T>(this T[,] array, int x1, int y1, int x2, int y2)
		{
			T[,] subArray = new T[y2 - y1, x2 - x1];
			for (int x = 0; x < x2 - x1; x++)
			{
				for (int y = 0; y < y2 - y1; y++)
				{
					try
					{
						subArray[y, x] = array[y + y1, x + x1];
					}
					catch(IndexOutOfRangeException)
					{

                    }
				}
			}
			return subArray;
		}

        public static int[,] SubArray(this int[,] array, int x1, int y1, int x2, int y2)
        {
            int[,] subArray = new int[y2 - y1, x2 - x1];
            for (int x = 0; x < x2 - x1; x++)
            {
                for (int y = 0; y < y2 - y1; y++)
                {
                    try
                    {
                        subArray[y, x] = array[y + y1, x + x1];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        subArray[y, x] = -1;
                    }
                }
            }
            return subArray;
        }

        public static double[] ConvertToDouble<T>(this T[] array)
        {
            double[] ar = new double[array.Length];
            for(int i = 0; i < ar.Length; i++)
            {
                ar[i] = System.Convert.ToDouble(array[i]);
            }
            return ar;
        }

        public static int[] ConvertToInt<T>(this T[] array)
        {
            int[] ar = new int[array.Length];
            for (int i = 0; i < ar.Length; i++)
            {
                ar[i] = System.Convert.ToInt32(array[i]);
            }
            return ar;
        }

        public static string[] ConvertToString<T>(this T[] array)
        {
            string[] ar = new string[array.Length];
            for (int i = 0; i < ar.Length; i++)
            {
                ar[i] = System.Convert.ToString(array[i]);
            }
            return ar;
        }

        public static double[,] ConvertToDouble<T>(this T[,] array)
        {
            int rs = array.GetLength(0);
            int cs = array.GetLength(1);
            double[,] ar = new double[rs, cs];
            for (int i = 0; i < rs; i++)
            {
                for(int j = 0; j < cs; j++)
                {
                    ar[i, j] = System.Convert.ToDouble(array[i, j]);
                }
            }
            return ar;
        }

        public static int[,] ConvertToInt<T>(this T[,] array)
        {
            int rs = array.GetLength(0);
            int cs = array.GetLength(1);
            int[,] ar = new int[rs, cs];
            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    ar[i, j] = System.Convert.ToInt32(array[i, j]);
                }
            }
            return ar;
        }

        public static string[,] ConvertToString<T>(this T[,] array)
        {
            int rs = array.GetLength(0);
            int cs = array.GetLength(1);
            string[,] ar = new string[rs, cs];
            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    ar[i, j] = System.Convert.ToString(array[i, j]);
                }
            }
            return ar;
        }

        public static T Max<T>(this T[,] array)
        {
            return array.Cast<T>().Max();
        }

        public static T Max<T>(this T[,,] array)
        {
            return array.Cast<T>().Max();
        }

        public static T Max<T>(this T[,,,] array)
        {
            return array.Cast<T>().Max();
        }

        public static T Max<T>(this T[][] array)
        {
            return array.Cast<T>().Max();
        }

        public static T Max<T>(this T[][][] array)
        {
            return array.Cast<T>().Max();
        }

        public static T Max<T>(this T[][][][] array)
        {
            return array.Cast<T>().Max();
        }

        public static T Min<T>(this T[,] array)
        {
            return array.Cast<T>().Min();
        }

        public static T Min<T>(this T[,,] array)
        {
            return array.Cast<T>().Min();
        }

        public static T Min<T>(this T[,,,] array)
        {
            return array.Cast<T>().Min();
        }

        public static T Min<T>(this T[][] array)
        {
            return array.Cast<T>().Min();
        }

        public static T Min<T>(this T[][][] array)
        {
            return array.Cast<T>().Min();
        }

        public static T Min<T>(this T[][][][] array)
        {
            return array.Cast<T>().Min();
        }
    }
}
