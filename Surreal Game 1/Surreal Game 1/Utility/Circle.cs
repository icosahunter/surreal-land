﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Surreal_Game_1
{
    public class Circle
    {
        public Point center;
        public int radius;

        public Circle(Point _center, int _radius)
        {
            center = _center;
            radius = _radius;
        }

        public bool Intersects(Rectangle rect)
        {
            // From: https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
            int distX = Math.Abs(center.X - rect.Center.X);
            int distY = Math.Abs(center.Y - rect.Center.Y);
            Console.WriteLine("X dist " + distX + " Y dist " + distY);

            if (distX > (rect.Width / 2 + radius)) { return false; }
            if (distY > (rect.Height / 2 + radius)) { return false; }

            if (distX <= (rect.Width / 2)) { return true; }
            if (distY <= (rect.Height / 2)) { return true; }

            return ((distX - rect.Width / 2) ^ 2 + (distY - rect.Height / 2) ^ 2) <= (radius ^ 2);
        }
        public bool Intersects(Circle circ)
        {
            return Math.Sqrt((center.X - circ.center.X) ^ 2 + (center.Y - circ.center.Y) ^ 2) < (radius + circ.radius);
        }
        public bool Intersects(Point p)
        {
            return Math.Sqrt((center.X - p.X) ^ 2 + (center.Y - p.Y) ^ 2) < radius;
        }
        public bool Intersects(Point p, Point size)
        {
            int distX = Math.Abs(center.X - (p.X + size.X/2));
            int distY = Math.Abs(center.Y - (p.Y + size.Y/2));
            Console.WriteLine("X dist " + distX + " Y dist " + distY);

            if (distX > (size.X / 2 + radius)) { return false; }
            if (distY > (size.Y / 2 + radius)) { return false; }

            if (distX <= (size.X / 2)) { return true; }
            if (distY <= (size.Y / 2)) { return true; }

            return ((distX - size.X / 2) ^ 2 + (distY - size.Y / 2) ^ 2) <= (radius ^ 2);
        }
    }
}
