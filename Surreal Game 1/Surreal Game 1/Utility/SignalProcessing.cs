﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public static class SignalProcessing
    {
        public delegate double[] WindowFunction1D(double[] array);
        public delegate double[,] WindowFunction2D(double[,] array);

        public delegate double ResamplingFunction1D(double[] array, int I, double lengthRatio);
        public delegate double ResamplingFunction2D(double[,] array, int X, int Y, double rowRatio, double colRatio);

        static Random rand = new Random();

        public static double[] Identity(double[] data)
        {
            return data;
        }

        public static double[,] Identity(double[,] data)
        {
            return data;
        }

        public static double PointSampling(double[] array, int I, double lengthRatio)
        {
            return array[(int)Math.Floor(I * lengthRatio)];
        }

        public static double PointSampling(double[,] array, int X, int Y, double rowRatio, double colRatio)
        {
            return array[(int)Math.Floor(Y * rowRatio), (int)Math.Floor(X * colRatio)];
        }

        public static double BilinearSampling(double[] array, int I, double lengthRatio)
        {
            return Average(BilinearWindow(array.SubArray((int)(I * lengthRatio), (int)(I * lengthRatio + lengthRatio))));
        }

        public static double BilinearSampling(double[,] array, int X, int Y, double rowRatio, double colRatio)
        {
            return Average(BilinearWindow(array.SubArray((int)(X * colRatio), (int)(Y * rowRatio), (int)(X * colRatio + colRatio), (int)(Y * rowRatio + rowRatio))));
        }

        public static double[,] BilinearWindow(double [,] data)
        {
            return data.Multiply(RadialGradient(1, 0, data.GetLength(0), data.GetLength(1)));
        }

        public static double[] BilinearWindow(double[] data)
        {
            return data.Multiply(RadialGradient(1, 0, data.Length));
        }

        public static double[] RandomNoise(int length)
        {
            return (new double[length]).Select(x => rand.NextDouble()).ToArray();
        }

        public static double[,] RandomNoise(int rows, int columns)
        {
            double[,] ar = new double[rows, columns];
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    ar[i, j] = rand.NextDouble();
                }
            }
            return ar;
        }

        public static double[] Gradient(double start, double end, int length)
        {
            double[] ar = new double[length];
            double d = (start - end);
            return ar.Select((x, i) => d * ((double)(length - i) / (double)length) + end).ToArray();
        }

        public static double[,] VerticalGradient(double start, double end, int rows, int columns)
        {
            double[,] ar = Ones(rows, columns);
            return ar.MultiplyByRows(Gradient(start, end, rows));
        }

        public static double[,] HorizontalGradient(double start, double end, int rows, int columns)
        {
            double[,] ar = Ones(rows, columns);
            return ar.MultiplyByColumns(Gradient(start, end, columns));
        }

        public static double[] RadialGradient(double start, double end, int length)
        {
            double[] ar = new double[length];
            double d = (start - end);
            int c = length / 2;
            return ar.Select((x, i) => d * ((double)(length - Math.Abs(c - i)) / (double)length) + end).ToArray();
        }

        public static double[,] RadialGradient(double start, double end, int rows, int columns)
        {
            int rowc = rows / 2;
            int colc = columns / 2;
            double length = Math.Max(rowc, colc);
            double[,] ar = new double[rows, columns];
            double d = (start - end);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    double distance = dist(j, i, colc, rowc);
                    ar[i, j] = d * ((length - distance) / length) + end;
                }
            }
            return ar;
        }

        public static double[,] RectangularGradient(double start, double end, int rows, int columns)
        {
            int rowc = rows / 2;
            int colc = columns / 2;
            double[,] ar = new double[rows, columns];
            double d = (start - end);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    ar[i, j] = d * Math.Max((Math.Abs(rowc - i) / (double)rowc), (Math.Abs(colc - j) / (double)colc)) + end;
                }
            }
            return ar;
        }

        private static double dist(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
        }

        public static double[] Ones(int length)
        {
            return (new double[length]).Select(x => 1d).ToArray();
        }

        public static double[,] Ones(int rows, int columns)
        {
            double[,] ar = new double[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    ar[i, j] = 1;
                }
            }
            return ar;
        }

        public static double[] Resize(double[] data, int length, ResamplingFunction1D sampling = null)
        {
            if (sampling == null)
            {
                sampling = PointSampling;
            }
            double[] ar = new double[length];
            double L = 0.99999999 * (double)data.Length / length;
            return ar.Select((x, i) => sampling(data, i, L)).ToArray();
        }

        public static double[,] Resize(double[,] data, int rows, int columns, ResamplingFunction2D sampling = null)
        {
            if (sampling == null)
            {
                sampling = PointSampling;
            }

            double[,] ar = new double[rows, columns];

            double C = 0.99999999 * (double)data.GetLength(1) / columns;
            double R = 0.99999999 * (double)data.GetLength(0) / rows;

            for(int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    ar[i, j] = sampling(data, j, i, R, C);
                }
            }

            return ar;
        }

        public static double[] SineSmooth(double[] data, int length)
        {
            double[] ar = new double[length];
            double R = (double)data.Length / (double)length;
            double r;
            double s;
            double h = 0;
            int j;
            for(int i = 0; i < length; i++)
            {
                s = i * R;
                r = s - Math.Floor(s) - 0.5;
                j = (int)Math.Floor(s);
                if (j + 1 < data.Length)
                {
                    h = Math.Abs(data[j + 1] - data[j]);
                }
                ar[i] = h * ModifSine(r);
            }

            return ar;
        }

        private static double ModifSine(double n)
        {
            return 0.5 + 0.5 * Math.Sin(Math.PI * n + (Math.PI / 2));
        }

        public static double[] AverageSmooth(double[] data, int averagingWidth = 3, int passes = 1, bool progressive = false, WindowFunction1D windowFunc = null)
        {
            double[] ar;

            if (windowFunc == null)
            {
                windowFunc = Identity;
            }

            if (progressive)
            {
                ar = (double[])data.Clone();
            }
            else
            {
                ar = new double[data.Length];
            }

            int s = ((averagingWidth - 1) / 2);

            for (int c = 0; c < passes; c++)
            {
                for (int i = 0; i < ar.Length; i++)
                {
                    ar[i] = Average(windowFunc(data.SubArray(i - s, i + s)));
                }
            }

            return ar;
        }

        public static double[,] AverageSmooth(double[,] data, int averagingSize = 3, int passes = 1, bool progressive = false, WindowFunction2D windowFunc = null)
        {
            int rs = data.GetLength(0);
            int cs = data.GetLength(1);

            if (windowFunc == null)
            {
                windowFunc = Identity;
            }

            double[,] ar;

            if (progressive)
            {
                ar = (double[,])data.Clone();
            }
            else
            {
                ar = new double[rs, cs];
            }

            int s = ((averagingSize - 1) / 2);

            double[,] temp;

            for (int c = 0; c < passes; c++)
            {
                for (int i = 0; i < rs; i++)
                {
                    for (int j = 0; j < cs; j++)
                    {
                        temp = data.SubArray(j - s, i - s, j + s, i + s);
                        temp = windowFunc(temp);
                        ar[i, j] = Average(temp);
                    }
                }
            }

            return ar;
        }

        public static double[] MedianSmooth(double[] data, int averagingWidth = 3, int passes = 1, bool progressive = false, WindowFunction1D windowFunc = null)
        {
            double[] ar;

            if (windowFunc == null)
            {
                windowFunc = Identity;
            }

            if (progressive)
            {
                ar = (double[])data.Clone();
            }
            else
            {
                ar = new double[data.Length];
            }

            int s = ((averagingWidth - 1) / 2);

            for (int c = 0; c < passes; c++)
            {
                for (int i = 0; i < ar.Length; i++)
                {
                    ar[i] = Median(windowFunc(data.SubArray(i - s, i + s)));
                }
            }

            return ar;
        }

        public static double[,] MedianSmooth(double[,] data, int averagingSize = 3, int passes = 1, bool progressive = false, WindowFunction2D windowFunc = null)
        {
            int rs = data.GetLength(0);
            int cs = data.GetLength(1);

            if (windowFunc == null)
            {
                windowFunc = Identity;
            }

            double[,] ar;

            if (progressive)
            {
                ar = (double[,])data.Clone();
            }
            else
            {
                ar = new double[rs, cs];
            }

            int s = ((averagingSize - 1) / 2);

            double[,] temp;

            for (int c = 0; c < passes; c++)
            {
                for (int i = 0; i < rs; i++)
                {
                    for (int j = 0; j < cs; j++)
                    {
                        temp = data.SubArray(j - s, i - s, j + s, i + s);
                        temp = windowFunc(temp);
                        ar[i, j] = Median(temp);
                    }
                }
            }

            return ar;
        }

        public static int[] Discretize(double[] data, int numOfIntervals)
        {
            double inter = (data.Max() - data.Min()) / numOfIntervals;
            return data.Select(x => (int)Math.Floor(x / inter)).ToArray();
        }

        public static int[] Discretize(double[] data, double[] stops)
        {
            List<double> sumStops = CumulitiveSum(stops).ToList();
            return data.Select(x => sumStops.FindLastIndex(a => x < a)).ToArray();
        }

        public static int[,] Discretize(double[,] data, int numOfIntervals)
        {
            double[,] normData = Normalize(data);
            double[] temp = normData.Cast<double>().ToArray();
            int rows = data.GetLength(0);
            int columns = data.GetLength(1);
            int[,] ar = new int[rows, columns];
            double inter = 0.00000001 + 1 / (double)numOfIntervals;
            
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    ar[i, j] = (int)Math.Floor(normData[i, j] / inter);
                }
            }
            return ar;
        }

        public static int[,] Discretize(double[,] data, double[] stops)
        {
            List<double> doubleList = stops.ToList();
            double[,] normData = Normalize(data);
            double[] temp = data.Cast<double>().ToArray();
            int rows = data.GetLength(0);
            int columns = data.GetLength(1);
            int[,] ar = new int[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    ar[i, j] = doubleList.FindIndex(a => normData[i, j] <= a);
                }
            }
            return ar;
        }

        public static double[] Normalize(double[] data)
        {
            return data.Multiply(1 / data.Max());
        }

        public static double[,] Normalize(double[,] data)
        {
            return data.Multiply(1 / data.Cast<double>().Max());
        }

        public static double Average(double[] data)
        {
            double sum = 0;
            for(int i = 0; i < data.Length; i++)
            {
                sum += data[i];
            }

            return sum / data.Length;
        }

        public static double Average(double[,] data)
        {
            double sum = 0;
            int rs = data.GetLength(0);
            int cs = data.GetLength(1);

            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    sum += data[i, j];
                }
            }

            return sum / data.Length;
        }

        public static double Median(double[] data)
        {
            double[] ar = (double[])data.Clone();
            Array.Sort(ar);
            return ar[data.Length / 2];
        }

        public static double Median(double[,] data)
        {
            return Median(data.Cast<double>().ToArray());
        }

        public static double[] CumulitiveSum(double[] data)
        {
            double[] ar = new double[data.Length];
            ar[0] = data[0];
            for(int i = 1; i < data.Length; i++)
            {
                ar[i] = ar[i - 1] + data[i];
            }
            return ar;
        }

        public static double[] Composite(double[] data1, double[] data2)
        {
            double[] ar = (double[])data1.Clone();
            for (int i = 0; i < data1.Length; i++)
            {
                if (data2[i] != 0)
                {
                    ar[i] = data2[i];
                }
            }
            return ar;
        }

        public static double[,] Composite(double[,] data1, double[,] data2)
        {
            int rs = data1.GetLength(0);
            int cs = data1.GetLength(1);
            double[,] ar = (double[,])data1.Clone();
            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    if (data2[i, j] != 0)
                    {
                        ar[i, j] = data2[i, j];
                    }
                }
            }
            return ar;
        }
        
        public static double[] Exponential(double[] data)
        {
            return data.Select(x => Math.Exp(x)).ToArray();
        }

        public static double[,] Exponential(double[,] data)
        {
            int rs = data.GetLength(0);
            int cs = data.GetLength(1);
            double[,] ar = (double[,])data.Clone();
            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    ar[i, j] = Math.Exp(data[i, j]);
                }
            }
            return ar;
        }

        public static double[] NatLog(double[] data)
        {
            return data.Select(x => Math.Log(x)).ToArray();
        }

        public static double[,] NatLog(double[,] data)
        {
            int rs = data.GetLength(0);
            int cs = data.GetLength(1);
            double[,] ar = (double[,])data.Clone();
            for (int i = 0; i < rs; i++)
            {
                for (int j = 0; j < cs; j++)
                {
                    ar[i, j] = Math.Log(data[i, j]);
                }
            }
            return ar;
        }
    }
}
