﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    static class MouseStateExtensions
    {
        public static bool IsButtonDown(this MouseState m, Inputs.MouseButton b)
        {
            switch (b)
            {
                case Inputs.MouseButton.LEFT: return m.LeftButton == ButtonState.Pressed;
                case Inputs.MouseButton.RIGHT: return m.RightButton == ButtonState.Pressed;
                case Inputs.MouseButton.MIDDLE: return m.MiddleButton == ButtonState.Pressed;
                case Inputs.MouseButton.XBUTTON1: return m.XButton1 == ButtonState.Pressed;
                case Inputs.MouseButton.XBUTTON2: return m.XButton2 == ButtonState.Pressed;
                default: return false;
            }
        }

        public static bool IsButtonUp(this MouseState m, Inputs.MouseButton b)
        {
            switch (b)
            {
                case Inputs.MouseButton.LEFT: return m.LeftButton == ButtonState.Released;
                case Inputs.MouseButton.RIGHT: return m.RightButton == ButtonState.Released;
                case Inputs.MouseButton.MIDDLE: return m.MiddleButton == ButtonState.Released;
                case Inputs.MouseButton.XBUTTON1: return m.XButton1 == ButtonState.Released;
                case Inputs.MouseButton.XBUTTON2: return m.XButton2 == ButtonState.Released;
                default: return false;
            }
        }
    }
}
