﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1.Utility
{
    static class HelpfulMathFunctions
    {
        public static double SimpleBellCurve(double x)
        {
            return 1 - Math.Pow(x, 2) / (Math.Pow(x, 2) + 1);
        }

        public static double InverseSimpleBellCurve(double x)
        {
            return Math.Pow(x, 2) / (Math.Pow(x, 2) + 1);
        }
    }
}
