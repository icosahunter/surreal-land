﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Plot : UIElement
    {
        public Point dataSize = new Point(0, 0);
        public Rectangle pos = new Rectangle(0,0,0,0);
        public Texture2D plottedData;
        public GraphicsDevice graphics;
        double[,] data;

        public Plot(Rectangle position, GraphicsDevice device)
        {
            pos = position;
            graphics = device;
            AddChild(new Image(plottedData));
        }

        public void AddData(double[,] set)
        {
            if (data == null)
            {
                data = set;
                dataSize = new Point(data.GetLength(1), data.GetLength(0));
                plottedData = new Texture2D(graphics, dataSize.X, dataSize.Y);
            }
            else
            {
                SignalProcessing.Composite(data, set);
            }
            UpdateTexture();
        }

        public void SetData(double[,] set)
        {
            data = set;
            dataSize = new Point(data.GetLength(1), data.GetLength(0));
            plottedData = new Texture2D(graphics, dataSize.X, dataSize.Y);
            UpdateTexture();
        }

        public void Clear()
        {
            data = null;
            dataSize = new Point(0, 0);
            plottedData = new Texture2D(graphics, dataSize.X, dataSize.Y);
            UpdateTexture();
        }

        public void UpdateTexture()
        {
            int l;
            for (int i = 0; i < dataSize.Y; i++)
            {
                for (int j = 0; j < dataSize.X; j++)
                {
                    l = (int)((data[i, j] / data.Max()) * 255);
                    Color[] c = { new Color(l, l, l) };
                    plottedData.SetData(0, new Rectangle(j, i, 1, 1), c, 0, 1);
                }
            }
        }

        public int indexFromCoords(int X, int Y)
        {
            return dataSize.X * (Y - 1) + X;
        }
    }
}
