﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public static class DefaultContent
    {
        static public Tile XTile;
        static public Image OutlineBlock;
        static public Texture2D BlankTexture;
    }
}
