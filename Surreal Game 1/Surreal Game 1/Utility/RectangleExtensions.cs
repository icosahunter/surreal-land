﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    static class RectangleExtensions
    {
        public static List<Point> GetCorners(this Rectangle rect)
        {
            List<Point> corners = new List<Point>();
            Point upperLeft = new Point(rect.X, rect.Y);
            Point upperRight = new Point(rect.X + rect.Width, rect.Y);
            Point lowerLeft = new Point(rect.X, rect.Y + rect.Height);
            Point lowerRight = new Point(rect.X + rect.Width, rect.Y + rect.Height);
            corners.Add(upperLeft);
            corners.Add(upperRight);
            corners.Add(lowerLeft);
            corners.Add(lowerRight);
            return corners;
        }
    }
}
