﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public static class Vector2Extensions
    {
        public static Vector2 MultipliedBy(this Vector2 v, float d)
        {
            return new Vector2(d * v.X, d * v.Y);
        }

        public static Vector2 DividedBy(this Vector2 v, float d)
        {
            return new Vector2(v.X / d, v.Y / d);
        }

        public static Vector2 AbsoluteVal(this Vector2 v)
        {
            return new Vector2(Math.Abs(v.X), Math.Abs(v.Y));
        }

        public static Vector2 GetUnitVector(this Vector2 v)
        {
            if (v.Length() > 0)
            {
                return v / v.Length();
            }
            else
            {
                return new Vector2(v.X, v.Y);
            }
        }

        public static Point ToPoint(this Vector2 v) { return new Point((int)Math.Round(v.X), (int)Math.Round(v.Y)); }

        public static double DotProduct(this Vector2 v1, Vector2 v2)
        {
            return (double)(v1.X*v2.X + v1.Y*v2.Y);
        }

        public static Vector2 GetProjection(this Vector2 v1, Vector2 v2)
        {
            if (v1.Length() != 0)
                return (float)DotProduct(v1, v2) * v1 / v1.LengthSquared();
            else
                return new Vector2(0,0);
        }
    }
}
