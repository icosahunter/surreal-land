﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public static class ListExtensions
    {
        private static Random rand = new Random();
        public static T Random<T>(this List<T> list)
        {
            int select = Convert.ToInt32(Math.Floor(list.Count() * rand.NextDouble()));
            return list[select];
        }
    }
}
