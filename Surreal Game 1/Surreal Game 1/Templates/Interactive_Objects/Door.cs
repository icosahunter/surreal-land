﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Door : Element
    {
        public Door(Point _position, IUIElement defaultImage, Point _size, string _description, Room room) : base(false, _position, defaultImage, Material.Default, _size, _description)
        {
            string interactButtons = InputController.GetFormattedAssignmentString(InteractionTypes.INTERACT);
            string interactPrompt = "Press " + interactButtons + " to enter.";
            triggers.Add(Triggers.PlayerInteractionAlertTrigger(interactPrompt));
            triggers.Add(Triggers.PlayerInteractionGoToNewRoom(room));
        }
    }
}
