﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public enum InteractionTypes { MOVE_UP, MOVE_DOWN, MOVE_RIGHT, MOVE_LEFT, JUMP, INTERACT, CLICK, ALT_CLICK }
    /// <summary>
    /// Represents a mapping between user defined identifiers and input identifiers.
    /// 
    /// An enumerated value "Interaction Types" is provided as an option for defined identifiers
    /// 
    /// Accepted Input identifiers include:
    /// Monogame.Xna.Framework.Input.Keys (Keyboard Keys)
    /// Monogame.Xna.Framework.Input.Buttons (Gamepad buttons)
    /// Surreal_Game_1.Inputs.MouseButton (Mouse Buttons)
    /// </summary>
    static class InputController
    {
        struct Assignment
        {
            public Assignment(object interaction_, object button_)
            {
                interaction = interaction_;
                button = button_;
            }

            public object interaction;
            public object button;
        }

        static List<Assignment> assignments = new List<Assignment>();

        /// <summary>
        /// Assign an identifier to an input
        /// </summary>
        /// <param name="interaction">identifier</param>
        /// <param name="button">input</param>
        static public void Assign(object interaction, object button)
        {
            assignments.Add(new Assignment(interaction, button));
        }

        static public object[] GetAssignments(object interaction)
        {
            return assignments.FindAll(x => x.interaction.Equals(interaction)).Select(x => x.button).ToArray();
        }

        static public string[] GetAssignmentStrings(object interaction)
        {
            return assignments.FindAll(x => x.interaction.Equals(interaction)).Select(x => x.button.ToString()).ToArray();
        }

        static public string GetFormattedAssignmentString(object interaction, int listLim = 3)
        {
            string curstr = "";
            string[] strs = GetAssignmentStrings(interaction);
            for(int i = 0; i < listLim && i < strs.Length; i++)
            {
                curstr += strs[i] + " or ";
            }
            
            if (curstr == "")
            {
                curstr = "[no assigned key]";
            }

            return curstr.Substring(0, curstr.Length - 4);
        }

        // Pressed and Released are not working correctly due to frame rate
        // Fix 1: Create a separate thread for pressed and released
        // Fix 2: Use events to register the presses and releases later

        /// <summary>
        /// Was the input with the provided identifier just pressed
        /// </summary>
        /// <param name="interaction">identifier</param>
        /// <returns></returns>
        static public bool WasButtonPressed(object interaction)
        {

            foreach (Assignment a in assignments.FindAll(x => x.interaction.Equals(interaction)))
            {
                if (Inputs.WasPressed(a.button))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Was the input with the provided identifier just released from a "pressed" state
        /// </summary>
        /// <param name="interaction">identifier</param>
        /// <returns></returns>
        static public bool WasButtonReleased(object interaction)
        {
            foreach (Assignment a in assignments.FindAll(x => x.interaction.Equals(interaction)))
            {
                if (Inputs.WasReleased(a.button))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Is the input with the provided identifier currently down
        /// </summary>
        /// <param name="interaction"></param>
        /// <returns></returns>
        static public bool IsButtonDown(object interaction)
        {
            foreach (Assignment a in assignments.FindAll(x => x.interaction.Equals(interaction)))
            {
                if (Inputs.IsButtonDown(a.button))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Is the input with the provided identifier currently up
        /// </summary>
        /// <param name="interaction"></param>
        /// <returns></returns>
        static public bool IsButtonUp(object interaction)
        {
            foreach (Assignment a in assignments.FindAll(x => x.interaction.Equals(interaction)))
            {
                if (Inputs.IsButtonUp(a.button))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
