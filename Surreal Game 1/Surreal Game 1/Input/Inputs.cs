﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    /// <summary>
    /// Handles inputs.
    /// Window instantiations contain and update an Inputs.
    /// Accepted Input identifiers include:
    /// Monogame.Xna.Framework.Input.Keys (Keyboard Keys)
    /// Monogame.Xna.Framework.Input.Buttons (Gamepad buttons)
    /// Surreal_Game_1.Inputs.MouseButton (Mouse Buttons)
    /// </summary>
    static class Inputs
    {
        static bool gamePadLastConnectionState = false;
        static Dictionary<Keys, bool> lastKeyStates = new Dictionary<Keys, bool>();
        static Dictionary<Buttons, bool> lastButtonStates = new Dictionary<Buttons, bool>();
        static Dictionary<MouseButton, bool> lastMouseButtonStates = new Dictionary<MouseButton, bool>();

        static Dictionary<Keys, bool> currentKeyStates = new Dictionary<Keys, bool>();
        static Dictionary<Buttons, bool> currentButtonStates = new Dictionary<Buttons, bool>();
        static Dictionary<MouseButton, bool> currentMouseButtonStates = new Dictionary<MouseButton, bool>();
        public enum MouseButton { LEFT, RIGHT, MIDDLE, XBUTTON1, XBUTTON2 };
        public static Point MousePosition
        {
            get
            {
                return Mouse.GetState().Position;
            }
        }

        static Inputs()
        {
            SetupMouse();
            SetupKeyboard();
            SetupGamePad();
        }

        /// <summary>
        /// Setup the necessary structures to handle mouse input
        /// This is done automatically on instantiation of Inputs
        /// and shouldn't normally need to be called again
        /// </summary>
        static public void SetupMouse()
        {
            lastMouseButtonStates.Clear();
            currentMouseButtonStates.Clear();
            foreach (MouseButton m in Enum.GetValues(typeof(MouseButton)))
            {
                lastMouseButtonStates.Add(m, false);
                currentMouseButtonStates.Add(m, false);
            }
        }

        /// <summary>
        /// Setup the necessary structures to handle keyboard input
        /// This is done automatically on instantiation of Inputs
        /// and shouldn't normally need to be called again
        /// </summary>
        static public void SetupKeyboard()
        {
            lastKeyStates.Clear();
            currentKeyStates.Clear();
            foreach (Keys k in Enum.GetValues(typeof(Keys)))
            {
                lastKeyStates.Add(k, false);
                currentKeyStates.Add(k, false);
            }
        }

        /// <summary>
        /// Setup the necessary structures to handle game controller input
        /// This is done automatically on instantiation of Inputs
        /// and shouldn't normally need to be called again
        /// 
        /// This function is automatically called in the Input.Update() function
        /// if a change in controller connection is detected
        /// </summary>
        static public void SetupGamePad()
        {
            lastButtonStates.Clear();
            currentButtonStates.Clear();
            GamePadCapabilities capabilites = GamePad.GetCapabilities(PlayerIndex.One);
            if (capabilites.HasAButton)
            {
                lastButtonStates.Add(Buttons.A, false);
                currentButtonStates.Add(Buttons.A, false);
            }
            if (capabilites.HasBackButton)
            {
                lastButtonStates.Add(Buttons.Back, false);
                currentButtonStates.Add(Buttons.Back, false);
            }
            if (capabilites.HasBButton)
            {
                lastButtonStates.Add(Buttons.B, false);
                currentButtonStates.Add(Buttons.B, false);
            }
            if (capabilites.HasBigButton)
            {
                lastButtonStates.Add(Buttons.BigButton, false);
                currentButtonStates.Add(Buttons.BigButton, false);
            }
            if (capabilites.HasDPadDownButton)
            {
                lastButtonStates.Add(Buttons.DPadDown, false);
                currentButtonStates.Add(Buttons.DPadDown, false);
            }
            if (capabilites.HasDPadLeftButton)
            {
                lastButtonStates.Add(Buttons.DPadLeft, false);
                currentButtonStates.Add(Buttons.DPadLeft, false);
            }
            if (capabilites.HasDPadRightButton)
            {
                lastButtonStates.Add(Buttons.DPadRight, false);
                currentButtonStates.Add(Buttons.DPadRight, false);
            }
            if (capabilites.HasDPadUpButton)
            {
                lastButtonStates.Add(Buttons.DPadUp, false);
                currentButtonStates.Add(Buttons.DPadUp, false);
            }
            if (capabilites.HasLeftShoulderButton)
            {
                lastButtonStates.Add(Buttons.LeftShoulder, false);
                currentButtonStates.Add(Buttons.LeftShoulder, false);
            }
            if (capabilites.HasLeftStickButton)
            {
                lastButtonStates.Add(Buttons.LeftStick, false);
                currentButtonStates.Add(Buttons.LeftStick, false);
            }
            if (capabilites.HasLeftTrigger)
            {
                lastButtonStates.Add(Buttons.LeftTrigger, false);
                currentButtonStates.Add(Buttons.LeftTrigger, false);
            }
            if (capabilites.HasRightShoulderButton)
            {
                lastButtonStates.Add(Buttons.RightShoulder, false);
                currentButtonStates.Add(Buttons.RightShoulder, false);
            }
            if (capabilites.HasRightStickButton)
            {
                lastButtonStates.Add(Buttons.RightStick, false);
                currentButtonStates.Add(Buttons.RightStick, false);
            }
            if (capabilites.HasRightTrigger)
            {
                lastButtonStates.Add(Buttons.RightTrigger, false);
                currentButtonStates.Add(Buttons.RightTrigger, false);
            }
            if (capabilites.HasStartButton)
            {
                lastButtonStates.Add(Buttons.Start, false);
                currentButtonStates.Add(Buttons.Start, false);
            }
            if (capabilites.HasXButton)
            {
                lastButtonStates.Add(Buttons.X, false);
                currentButtonStates.Add(Buttons.X, false);
            }
            if (capabilites.HasYButton)
            {
                lastButtonStates.Add(Buttons.Y, false);
                currentButtonStates.Add(Buttons.Y, false);
            }
            if (capabilites.HasRightXThumbStick)
            {
                lastButtonStates.Add(Buttons.RightThumbstickLeft, false);
                lastButtonStates.Add(Buttons.RightThumbstickRight, false);

                currentButtonStates.Add(Buttons.RightThumbstickLeft, false);
                currentButtonStates.Add(Buttons.RightThumbstickRight, false);
            }
            if (capabilites.HasRightYThumbStick)
            {
                lastButtonStates.Add(Buttons.RightThumbstickUp, false);
                lastButtonStates.Add(Buttons.RightThumbstickDown, false);

                currentButtonStates.Add(Buttons.RightThumbstickUp, false);
                currentButtonStates.Add(Buttons.RightThumbstickDown, false);
            }
            if (capabilites.HasLeftXThumbStick)
            {
                lastButtonStates.Add(Buttons.LeftThumbstickRight, false);
                lastButtonStates.Add(Buttons.LeftThumbstickLeft, false);

                currentButtonStates.Add(Buttons.LeftThumbstickRight, false);
                currentButtonStates.Add(Buttons.LeftThumbstickLeft, false);
            }
            if (capabilites.HasLeftYThumbStick)
            {
                lastButtonStates.Add(Buttons.LeftThumbstickUp, false);
                lastButtonStates.Add(Buttons.LeftThumbstickDown, false);

                currentButtonStates.Add(Buttons.LeftThumbstickUp, false);
                currentButtonStates.Add(Buttons.LeftThumbstickDown, false);
            }
        }

        /// <summary>
        /// Update the last/current states of the inputs and update the game controller
        /// setup if a connection change is detected
        /// </summary>
        static public void Update()
        {
            GamePadState g = GamePad.GetState(PlayerIndex.One);

            if (gamePadLastConnectionState != g.IsConnected)
            {
                SetupGamePad();
            }

            gamePadLastConnectionState = g.IsConnected;

            foreach (Buttons b in currentButtonStates.Keys.ToList())
            {
                lastButtonStates[b] = currentButtonStates[b];
                currentButtonStates[b] = g.IsButtonDown(b);
            }

            KeyboardState kb = Keyboard.GetState();
            foreach(Keys k in currentKeyStates.Keys.ToList())
            {
                lastKeyStates[k] = currentKeyStates[k];
                currentKeyStates[k] = kb.IsKeyDown(k);
            }

            foreach(MouseButton mb in currentMouseButtonStates.Keys.ToList())
            {
                lastMouseButtonStates[mb] = currentMouseButtonStates[mb];
            }

            MouseState m = Mouse.GetState();
            currentMouseButtonStates[MouseButton.LEFT] = m.LeftButton == ButtonState.Pressed;
            currentMouseButtonStates[MouseButton.RIGHT] = m.RightButton == ButtonState.Pressed;
            currentMouseButtonStates[MouseButton.MIDDLE] = m.MiddleButton == ButtonState.Pressed;
            currentMouseButtonStates[MouseButton.XBUTTON1] = m.XButton1 == ButtonState.Pressed;
            currentMouseButtonStates[MouseButton.XBUTTON2] = m.XButton2 == ButtonState.Pressed;
        }

        /// <summary>
        /// Returns a list of the identifiers for any currently pressed inputs
        /// </summary>
        /// <returns></returns>
        static public List<object> GetPressedInputs()
        {
            Update();
            List<object> pressedInputs = new List<object>();
            foreach(Keys k in lastKeyStates.Keys)
            {
                if (lastKeyStates[k])
                {
                    pressedInputs.Add(k);
                }
            }
            foreach (Buttons b in lastButtonStates.Keys)
            {
                if (lastButtonStates[b])
                {
                    pressedInputs.Add(b);
                }
            }
            foreach (MouseButton m in lastMouseButtonStates.Keys)
            {
                if (lastMouseButtonStates[m])
                {
                    pressedInputs.Add(m);
                }
            }

            return pressedInputs;
        }

        /// <summary>
        /// Checks if the input with the given identifier was just released from a "pressed" state
        /// </summary>
        /// <param name="inputVal">Input Identifier</param>
        /// <returns></returns>
        static public bool WasReleased(object inputVal)
        {
            if (inputVal is Keys)
            {
                return lastKeyStates[(Keys)inputVal] == true && currentKeyStates[(Keys)inputVal];
            }
            else if (inputVal is Buttons)
            {
                return lastButtonStates[(Buttons)inputVal] == true && currentButtonStates[(Buttons)inputVal];
            }
            else if (inputVal is MouseButton)
            {
                return lastMouseButtonStates[(MouseButton)inputVal] == true && currentMouseButtonStates[(MouseButton)inputVal];
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the input with the given identifer was just pressed
        /// </summary>
        /// <param name="inputVal">Input Identifier</param>
        /// <returns></returns>
        static public bool WasPressed(object inputVal)
        {
            
            if (inputVal is Keys)
            {
                return lastKeyStates[(Keys)inputVal] == false && currentKeyStates[(Keys)inputVal];
            }
            else if (inputVal is Buttons)
            {
                return lastButtonStates[(Buttons)inputVal] == false && currentButtonStates[(Buttons)inputVal];
            }
            else if (inputVal is MouseButton)
            {
                return lastMouseButtonStates[(MouseButton)inputVal] == false && currentMouseButtonStates[(MouseButton)inputVal];
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the input with the given identifier is currently down
        /// </summary>
        /// <param name="inputVal">Input Identifier</param>
        /// <returns></returns>
        static public bool IsButtonDown(object inputVal)
        {
            if (inputVal is Keys)
            {
                return currentKeyStates[(Keys)inputVal];
            }
            else if (inputVal is Buttons)
            {
                return currentButtonStates[(Buttons)inputVal];
            }
            else if (inputVal is MouseButton)
            {
                return currentMouseButtonStates[(MouseButton)inputVal];
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the input with the given identifier is currently up
        /// </summary>
        /// <param name="inputVal">Input Identifier</param>
        /// <returns></returns>
        static public bool IsButtonUp(object inputVal)
        {
            if (inputVal is Keys)
            {
                return !currentKeyStates[(Keys)inputVal];
            }
            else if (inputVal is Buttons)
            {
                return !currentButtonStates[(Buttons)inputVal];
            }
            else if (inputVal is MouseButton)
            {
                return !currentMouseButtonStates[(MouseButton)inputVal];
            }
            else
            {
                return false;
            }
        }
    }
}
