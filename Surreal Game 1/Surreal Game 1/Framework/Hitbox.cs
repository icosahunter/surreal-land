﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public abstract class Hitbox
    {
        abstract public Point position { get; set; }
        abstract public int left { get; }
        abstract public int right { get; }
        abstract public int bottom { get; }
        abstract public int top { get; }
        abstract public int width { get; }
        abstract public int height { get; }
        abstract public Point center { get; }
        abstract public bool CollidesWith(Hitbox hitbox2);
        abstract public bool CollidesWith(Rectangle rect);
        abstract public Vector2 CollisionDirection(Hitbox hitbox2);
        abstract public Vector2 CollisionDirection(Rectangle rect);
        abstract public int area { get; }
        // NOTE: A HITBOX WIDTH OF 1 WILL COVER A MULTITUDE OF PHYSICS SINS UNTIL WE FULLY IMPLEMENT TRULY CLEAN AND GOOD COLLISION PREVENTION!
        abstract public void SetHitWidth(int w);
    }

    public class RectHitbox : Hitbox
    {
        public override Point position { get { return rectangle.Location; } set { rectangle.Location = value; } }
        public Rectangle rectangle;
        public int hitBorderWidth;
        public override int left { get { return rectangle.Left; } }
        public override int right { get { return rectangle.Right; } }
        public override int bottom { get { return rectangle.Bottom; } }
        public override int top { get { return rectangle.Top; } }
        public override int width { get { return rectangle.Width; } }
        public override int height { get { return rectangle.Height; } }
        public override int area { get { return rectangle.X * rectangle.Y; } }
        public override Point center { get { return rectangle.Center; } }

        public override void SetHitWidth(int w)
        {
            hitBorderWidth = w;
        }

        public RectHitbox(Point rect, int hitWidth)
        {
            rectangle = new Rectangle(new Point(0, 0), rect);
            hitBorderWidth = hitWidth;
        }

        public RectHitbox(Rectangle r, int hitWidth)
        {
            rectangle = r;
            hitBorderWidth = hitWidth;
        }

        public override bool CollidesWith(Hitbox hitbox2)
        {
            if (hitbox2 is RectHitbox)
            {
                return rectangle.Intersects(((RectHitbox)hitbox2).rectangle);
            }
            else
            {
                return ((CircHitbox)hitbox2).circle.Intersects(this.rectangle);
                throw new System.NotImplementedException();
            }
        }

        public override Vector2 CollisionDirection(Hitbox hitbox2)
        {
            if (hitbox2 is RectHitbox)
            {
                Rectangle top = new Rectangle(rectangle.X + hitBorderWidth, rectangle.Y, rectangle.Width - 2 * hitBorderWidth, hitBorderWidth);
                Rectangle right = new Rectangle(rectangle.Right - hitBorderWidth, rectangle.Y + hitBorderWidth, hitBorderWidth, rectangle.Height - 2 * hitBorderWidth);
                Rectangle bottom = new Rectangle(rectangle.X + hitBorderWidth, rectangle.Bottom - hitBorderWidth, rectangle.Width - 2 * hitBorderWidth, hitBorderWidth);
                Rectangle left = new Rectangle(rectangle.X, rectangle.Y + hitBorderWidth, hitBorderWidth, rectangle.Height - 2 * hitBorderWidth);
                Vector2 dir = new Vector2(0, 0);

                if (((RectHitbox)hitbox2).rectangle.Intersects(top))
                {
                    dir.Y += -1;
                }

                if (((RectHitbox)hitbox2).rectangle.Intersects(bottom))
                {
                    dir.Y += 1;
                }

                if (((RectHitbox)hitbox2).rectangle.Intersects(right))
                {
                    dir.X += 1;
                }

                if (((RectHitbox)hitbox2).rectangle.Intersects(left))
                {
                    dir.X += -1;
                }

                return dir;
            }
            else
            {
                // It may be better to take in the velocity so a circular hitbox collision direction can be calculated simply.
                throw new System.NotImplementedException();
            }
        }

        public override bool CollidesWith(Rectangle rect)
        {
            return rectangle.Intersects(rect);
        }

        public override Vector2 CollisionDirection(Rectangle rect)
        {
            Rectangle top = new Rectangle(rectangle.X + hitBorderWidth, rectangle.Y, rectangle.Width - 2 * hitBorderWidth, hitBorderWidth);
            Rectangle right = new Rectangle(rectangle.Right - hitBorderWidth, rectangle.Y + hitBorderWidth, hitBorderWidth, rectangle.Height - 2 * hitBorderWidth);
            Rectangle bottom = new Rectangle(rectangle.X + hitBorderWidth, rectangle.Bottom - hitBorderWidth, rectangle.Width - 2 * hitBorderWidth, hitBorderWidth);
            Rectangle left = new Rectangle(rectangle.X, rectangle.Y + hitBorderWidth, hitBorderWidth, rectangle.Height - 2 * hitBorderWidth);
            Vector2 dir = new Vector2(0, 0);

            if (rect.Intersects(top))
            {
                dir.Y += -1;
            }

            if (rect.Intersects(bottom))
            {
                dir.Y += 1;
            }

            if (rect.Intersects(right))
            {
                dir.X += 1;
            }

            if (rect.Intersects(left))
            {
                dir.X += -1;
            }

            return dir;
        }

            //if (Math.Abs(dist.Y) < Math.Abs(dist.X))
            //{
            //    if (dist.Y < 0)
            //    {
            //        return new Vector2(0, -1);
            //    }
            //    else
            //    {
            //        return new Vector2(0, 1);
            //    }
            //}
            //else
            //{
            //    if (dist.X < 0)
            //    {
            //        return new Vector2(1, 0);
            //    }
            //    else
            //    {
            //        return new Vector2(-1, 0);
            //    }
            //}
    }
    public class CircHitbox : Hitbox
    {
        public override Point position { get { return circle.center; } set { circle.center = value; } }
        public Circle circle;
        public int hitBorderWidth;
        public override int left { get { return circle.center.X - circle.radius; } }
        public override int right { get { return circle.center.X + circle.radius; } }
        public override int top { get { return circle.center.Y - circle.radius; } }
        public override int bottom { get { return circle.center.Y + circle.radius; } }
        public override int width { get { return circle.radius * 2; } }
        public override int height { get { return circle.radius * 2; } }
        public override Point center { get { return circle.center; } }
        public override int area { get
            {
                double accurateArea = Math.PI * (circle.radius ^ 2);
                if (accurateArea - Math.Floor(accurateArea) > 0.5)
                    return (int)Math.Ceiling(accurateArea);
                else
                    return (int)Math.Floor(accurateArea);
            }
        }

        public override void SetHitWidth(int w)
        {
            hitBorderWidth = w;
        }

        public CircHitbox(Circle _circle, int hitWidth)
        {
            circle = _circle;
            hitBorderWidth = hitWidth;
        }

        public override bool CollidesWith(Hitbox hitbox2)
        {
            if (hitbox2 is RectHitbox)
                return circle.Intersects(((RectHitbox)hitbox2).rectangle);
            else if (hitbox2 is CircHitbox)
                return circle.Intersects(((CircHitbox)hitbox2).circle);
            else
                throw new System.NotImplementedException();
        }

        public override bool CollidesWith(Rectangle rect)
        {
            return circle.Intersects(rect);
        }

        public override Vector2 CollisionDirection(Hitbox hitbox2)
        {
            throw new NotImplementedException();
        }

        public override Vector2 CollisionDirection(Rectangle rect)
        {
            throw new NotImplementedException();
        }
    }
}
