﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Entity : Element, IEntity
    {
        public Vector2 velocity { get { return new Vector2((float)XSpeed, (float)YSpeed); } set { XSpeed = value.X; YSpeed = value.Y; } }
        public double XSpeed { get; set; }
        public double YSpeed { get; set; }
        public Vector2 momentum { get { return (float)mass * velocity; } }
        public double XMomentum { get { return momentum.X; } }
        public double YMomentum { get { return momentum.Y; } }
        public double mass { get { return hitbox.area * material.density; } }
        public double maxSpeed { get; set; }
        public int damagePoints { get; set; }
        public double velocityDirection { get; set; }
        public double facingDirection { get { return fd; } set { fd = value % (2 * Math.PI); } }
        private double fd;
        public bool isMovingUp { get { return velocity.Y > 0; } }
        public bool isMovingDown { get { return velocity.Y < 0; } }
        public bool isMovingRight { get { return velocity.X > 0; } }
        public bool isMovingLeft { get { return velocity.X < 0; } }
        public bool isFacingUp { get { return 0 < facingDirection && facingDirection < Math.PI; }}
        public bool isFacingDown { get { return Math.PI < facingDirection && facingDirection < 2*Math.PI; } }
        public bool isFacingRight { get { return 3*Math.PI/2 < facingDirection || facingDirection < Math.PI/2; } }
        public bool isFacingLeft { get { return Math.PI/2 < facingDirection && facingDirection < 3*Math.PI/2; } }
        public bool isDestroyed { get { return damagePoints <= 0; } }

        public Entity(Hitbox _hitbox, bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description, int _damagePoints, float _maxSpeed, bool _facingRight, bool _blindsight) : base(_hitbox, _isSolid, _position, defaultImage, _material, _size, _description)
        {
            damagePoints = _damagePoints;
            maxSpeed = _maxSpeed;
        }

		public Entity(bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_isSolid, _position, defaultImage, _material, _size, _description)
        {

        }

		public Entity(Point _position, IUIElement defaultImage, Material _material, Point _size, string _description) : base(_position, defaultImage, _material, _size, _description)
        {

        }

		public Entity(Point _position, IUIElement defaultImage, Point _size, string _description) : base(_position, defaultImage, _size, _description)
        {

        }

		public Entity(IUIElement defaultImage, Point _size, string _description) : base(defaultImage, _size, _description)
        {

        }
    }
}
