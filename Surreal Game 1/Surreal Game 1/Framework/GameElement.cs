﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class GameElement : UIElement, IGameElement
    {
        public virtual Camera camera { get; set; }
        public virtual bool cameraMode { get; set; }
        public virtual Material material { get; set; }
        public virtual IUIElement image { get; set; }
        public virtual string description { get; set; }
        public virtual Hitbox hitbox { get; set; }
        public virtual bool topIsSolid { get; set; }
        public virtual bool bottomIsSolid { get; set; }
        public virtual bool rightIsSolid { get; set; }
        public virtual bool leftIsSolid { get; set; }
        public virtual Inventory inventory { get; set; }
        public virtual void SetHitboxPosition(Point position) { }

        public GameElement()
        {
            autoFormatting = false;
        }

        public override void Draw(SpriteBatch batch, Point pos, Point drawsize)
        {
            base.Draw(batch, pos, drawsize);

            if (autoFormatting)
            {
                if (cameraMode)
                {
                    image.Draw(batch, camera.AdjustForCameraPos(pos), drawsize);
                }
                else
                {
                    image.Draw(batch, pos, drawsize);
                }
            }
            else if (parent != null)
            {
                if(cameraMode)
                {
                    image.Draw(batch, camera.AdjustForCameraPos(parent.position + position), size);
                }
                else
                {
                    image.Draw(batch, parent.position + position, size);
                }
            }
            else
            {
                Draw(batch);
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);

            if(cameraMode)
            {
                image.Draw(batch, camera.AdjustForCameraPos(position), size);
            }
            else
            {
                image.Draw(batch, position, size);
            }
        }
    }
}
