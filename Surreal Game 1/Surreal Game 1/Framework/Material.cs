﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Material
    {
        public double roughness;
        public double elasticity;
        public double hardness;
        public double density;

        public static Material Default = new Material(0, 0, 1, 0.05);

        public Material(double _roughness, double _elasticity, double _hardness, double _density)
        {
            roughness = _roughness;
            elasticity = _elasticity;
            hardness = _hardness;
            density = _density;
        }
    }
}
