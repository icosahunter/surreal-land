﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class UIElement : IUIElement
    {
        public virtual Point size { get { return new Point(width, height); } set { width = value.X; height = value.Y; } }
        public virtual int width { get; set; }
        public virtual int height { get; set; }
        public virtual Point position { get { return new Point(X, Y); } set { X = value.X; Y = value.Y; } }
        public virtual int X { get; set; }
        public virtual int Y { get; set; }
        public virtual bool autoFormatting { get; set; }
        public virtual Point actualPosition { get { return new Point(actualX, actualY); } protected set { actualX = value.X; actualY = value.Y; } }
        public virtual int actualX { get; protected set; }
        public virtual int actualY { get; protected set; }
        public virtual Point actualSize { get { return new Point(actualWidth, actualHeight); } protected set { actualWidth = value.X; actualHeight = value.Y; } }
        public virtual int actualWidth { get; protected set; }
        public virtual int actualHeight { get; protected set; }
        public virtual IUIElement parent { get; set; }
        public virtual List<IUIElement> children { get; }
        public virtual List<Trigger> triggers { get; }
        public Point center { get { return actualPosition + actualSize.DividedBy(2); } }
        public Point upperLeft { get { return actualPosition; } }
        public Point upperRight { get { return new Point(right, top); } }
        public Point lowerLeft { get { return new Point(left, bottom); } }
        public Point lowerRight { get { return new Point(right, bottom); } }
        public int top { get { return actualPosition.Y; } }
        public int right { get { return actualPosition.X + actualSize.X; } }
        public int left { get { return actualPosition.X; } }
        public int bottom { get { return actualPosition.Y + actualSize.Y; } }
        public Rectangle boundingRectangle { get { return new Rectangle(position, size); } }
        public Rectangle actualBoundingRectangle { get { return new Rectangle(position, size); } }

        public UIElement()
        {
            children = new List<IUIElement>();
            triggers = new List<Trigger>();
        }

        public virtual void AddChild(IUIElement element)
        {
            element.parent = this;
            children.Add(element);
        }
        public virtual void RemoveChild(IUIElement element)
        {
            element.parent = null;
            children.Remove(element);
        }
        public virtual void AddTrigger(Trigger trigger)
        {
            triggers.Add(trigger);
        }
        public virtual void RemoveTrigger(Trigger trigger)
        {
            triggers.Remove(trigger);
        }

        public virtual void Draw(SpriteBatch batch)
        {
            actualPosition = position;
            actualSize = size;

            foreach (IUIElement e in children)
            {
                e.Draw(batch, actualPosition, actualSize);
            }
        }

        public virtual void Draw(SpriteBatch batch, Point pos, Point drawSize)
        {
            if (autoFormatting)
            {
                actualPosition = pos;
                actualSize = drawSize;
            }
            else
            {
                actualPosition = position;
                actualSize = size;
            }

            foreach(IUIElement e in children)
            {
                e.Draw(batch, actualPosition, actualSize);
            }
        }

        public virtual void Update()
        {
            foreach(IUIElement e in children)
            {
                e.Update();
            }
        }
    }
}
