﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Element : GameElement
    {
		public List<IUIElement> images = new List<IUIElement>();
        public int sightRange { set { circle.radius = value; }
            get { if (circle == null)
                    return 0;
                  else
                    return circle.radius; }
        }
        public Circle circle;

        public override Point position { get { return new Point(X, Y); } set { X = value.X; Y = value.Y; SyncPosition(); } }

        //private Condition c = delegate (object triggerOwner, object otherObject)
        //{
        //    return (triggerOwner != otherObject && triggerOwner is Element && otherObject is Element && ((Element)triggerOwner).circle.Intersects(((Element)otherObject).position, ((Element)otherObject).size));
        //};

        public Element(Hitbox _hitbox, bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description)
        {
            //triggers.Add(new Trigger(c, _result));
            description = _description;
            hitbox = _hitbox;
            if (_isSolid)
            {
                topIsSolid = true;
                bottomIsSolid = true;
                rightIsSolid = true;
                leftIsSolid = true;
            }
            position = _position;
            images.Add(defaultImage);
            image = defaultImage;
            material = _material;
            size = _size;
            camera = null;
            cameraMode = false;
            circle = new Circle(position, 0);
            SyncPosition();
        }

        public Element(bool _isSolid, Point _position, IUIElement defaultImage, Material _material, Point _size, string _description)
        {
            description = _description;
            hitbox = new RectHitbox(_size, Math.Min(_size.X, _size.Y)/3);
            topIsSolid = true;
            bottomIsSolid = true;
            rightIsSolid = true;
            leftIsSolid = true;
            position = _position;
            images.Add(defaultImage);
            image = defaultImage;
            material = _material;
            size = _size;
            camera = null;
            cameraMode = false;
            circle = new Circle(position, 0);
            SyncPosition();
        }

        public Element(Point _position, IUIElement defaultImage, Material _material, Point _size, string _description)
        {
            description = _description;
            hitbox = new RectHitbox(_size, Math.Min(_size.X, _size.Y) / 3);
            topIsSolid = true;
            bottomIsSolid = true;
            rightIsSolid = true;
            leftIsSolid = true;
            position = _position;
            images.Add(defaultImage);
            image = defaultImage;
            material = _material;
            size = _size;
            camera = null;
            cameraMode = false;
            circle = new Circle(position, 0);
            SyncPosition();
        }

        public Element(Point _position, IUIElement defaultImage, Point _size, string _description)
        {
            description = _description;
            hitbox = new RectHitbox(_size, Math.Min(_size.X, _size.Y) / 3);
            topIsSolid = true;
            bottomIsSolid = true;
            rightIsSolid = true;
            leftIsSolid = true;
            position = _position;
            images.Add(defaultImage);
            image = defaultImage;
            material = Material.Default;
            size = _size;
            camera = null;
            cameraMode = false;
            circle = new Circle(position, 0);
            SyncPosition();
        }
        
        public Element(IUIElement defaultImage, Point _size, string _description)
        {
            description = _description;
            hitbox = new RectHitbox(_size, Math.Min(_size.X, _size.Y) / 3);
            topIsSolid = true;
            bottomIsSolid = true;
            rightIsSolid = true;
            leftIsSolid = true;
            position = new Point(0, 0);
            images.Add(defaultImage);
            image = defaultImage;
            material = Material.Default;
            size = _size;
            camera = null;
            cameraMode = false;
            circle = new Circle(position, 0);
            SyncPosition();
        }

        public void AddImage(IUIElement img)
        {
            images.Add(img);
        }

        public void RemoveImage(IUIElement img)
        {
            images.Remove(img);
        }

        public void SetCurrentImage(int index)
        {
            image = images[index];
        }

        //public void AddAwarenessResult(Result r, Condition con = null)
        //{
        //    if (con == null) { con = c; }
        //    triggers.Add(new Trigger(con, r));
        //}

        public override void Update()
        {
            base.Update();
        }

        //public void CheckPosition(Object o, TileLoc loc = TileLoc.NONE)
        //{
        //    if (reaction != null && this != o && o is Element)
        //    {
        //        if (canSee(((Element)o).position, ((Element)o).size))
        //        {
        //            reaction();
        //        }
        //    }
        //    else if (reaction != null && o is Tile)
        //    {

        //    }
        //}

        //public void CheckPosition(Object o, TileLoc loc = TileLoc.NONE)
        //{
        //    if (reaction != null && this != o && o is Element)
        //    {
        //        if (canSee(((Element)o).position, ((Element)o).size))
        //        {
        //            reaction();
        //        }
        //    }
        //    else if (reaction != null && o is Tile)
        //    {

        //    }
        //}

        //private bool canSee(Point position, Point size) { return circle.Intersects(position, size); }

        private void SyncPosition()
        {
            hitbox.position = position;
            circle = new Circle(center, sightRange);
        }

        public void SetHitboxPosition(Point pos)
        {
            hitbox.position = pos;
            this.position = pos;
        }
    }

    public enum TileLoc
    {
        ABOVE = 0,
        LEVEL = 1,
        BELOW = 2,
        NONE = 3
    }
}
