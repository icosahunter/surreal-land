﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Camera
    {
        public Point position = new Point(0, 0);
        public Point relativePos = new Point(0, 0);
        public enum FollowType { CENTEREDX, CENTEREDY, CENTERED, CUSTOM, STATIC }
        public FollowType mode;
        public Element elementToFollow;
        public GameScreen screen;
        
        public Camera(GameScreen screen_, Point pos)
        {
            screen = screen_;
            position = pos;
            elementToFollow = null;
            mode = FollowType.STATIC;
        }

        public Camera(GameScreen screen_, Point pos, Element toFollow)
        {
            screen = screen_;
            position = pos;
            elementToFollow = toFollow;
            mode = FollowType.CENTERED;
        }

        public Camera(GameScreen screen_, Point pos, Element toFollow, FollowType mode_, Point relativePos_)
        {
            screen = screen_;
            position = pos;
            elementToFollow = toFollow;
            mode = mode_;
            relativePos = relativePos_;
        }

        public Point AdjustForCameraPos(Point pos)
        {
            int x = pos.X - position.X;
            int y = pos.Y - position.Y;
            return new Point(x, y);
        }

        public void Update()
        {
            if (mode == FollowType.CENTERED)
            {
                position.X = elementToFollow.position.X - (screen.window.width / 2);
                position.Y = elementToFollow.position.Y - (screen.window.height / 2);
            }
            else if (mode == FollowType.CENTEREDX)
            {
                position.X = elementToFollow.position.X - (screen.window.width / 2);
                position.Y = elementToFollow.position.Y - relativePos.Y;
            }
            else if (mode == FollowType.CENTEREDY)
            {
                position.Y = elementToFollow.position.Y - (screen.window.height / 2);
                position.X = elementToFollow.position.X - relativePos.X;
            }
            else if (mode == FollowType.CUSTOM)
            {
                position.X = elementToFollow.position.X - relativePos.X;
                position.Y = elementToFollow.position.Y - relativePos.Y;
            }
        }
    }
}
