﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Image : UIElement
	{
		public Texture2D defaultTexture;
        public Rectangle sourceRectangle;
        public Image(Texture2D image)
		{
			defaultTexture = image;
            size = new Point(defaultTexture.Width, defaultTexture.Height);
            autoFormatting = true;
            sourceRectangle = new Rectangle(new Point(0, 0), size);
        }

        public Image(Texture2D image, Rectangle sourceRect)
        {
            defaultTexture = image;
            size = new Point(sourceRect.Width, sourceRect.Height);
            autoFormatting = true;
            sourceRectangle = sourceRect;
        }

        public override void Draw(SpriteBatch batch, Point pos, Point drawsize)
		{
            base.Draw(batch, pos, drawsize);

            if (autoFormatting)
            {
                batch.Draw(defaultTexture, new Rectangle(pos, drawsize), sourceRectangle, Color.White);
            }
            else
            {
                batch.Draw(defaultTexture, new Rectangle(parent.position + position, size), sourceRectangle, Color.White);
            }
  		}

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch, actualPosition, actualSize);
        }
    }

    public class EmptyImage : UIElement
	{
        public EmptyImage(Point imgSize)
		{
			size = imgSize;
		}
	}

    public class AnimatedImage : Image
	{
		public override Point size { get { return sizeSet ? bufferSize : new Point(currentTexture.Width, currentTexture.Height); } set { bufferSize = value; sizeSet = true; } }
        private bool sizeSet;
        private Point bufferSize;
		public Texture2D currentTexture;
        public List<Texture2D> frames;
        bool isPlaying = false;
        int currentFrameIndex = 0;
        public long frameTime = 10;
		public long elapsedFrames;
        public bool loop = true;

		public AnimatedImage(Texture2D defaultImage) : base(defaultImage)
		{
			currentTexture = defaultImage;
			frames = new List<Texture2D>();
		}

		public void AddFrame(Texture2D image)
        {
            if (frames == null)
            {
                frames = new List<Texture2D>();
            }
            frames.Add(image);
        }

        public void StopAnimation()
        {
			isPlaying = false;
			currentFrameIndex = 0;
			elapsedFrames = 0;
        }

        public void PauseAnimation()
        {
			isPlaying = false;
        }

        public void StartAnimation()
        {
            isPlaying = true;
			elapsedFrames = 0;
        }

        public void NextFrame()
        {
			if (currentFrameIndex >= frames.Count - 1)
			{
                if (loop)
                {
                    currentFrameIndex = 0;
                }
                else
                {
                    StopAnimation();
                }
			}
			else
			{
				currentFrameIndex++;
			}
			currentTexture = frames[currentFrameIndex];
			elapsedFrames = 0;
        }

	    public override void Update()
		{
			if (isPlaying && elapsedFrames >= frameTime)
            {
                NextFrame();
            }
		}

		public override void Draw(SpriteBatch batch, Point pos, Point drawsize)
		{
            base.Draw(batch, pos, drawsize);

			if (isPlaying)
			{
				elapsedFrames++;    
			}

            if (autoFormatting)
            {
                batch.Draw(defaultTexture, new Rectangle(pos, drawsize), Color.White);
            }
            else if (parent != null)
            {
                batch.Draw(defaultTexture, new Rectangle(parent.position + position, size), Color.White);
            }
            else
            {
                Draw(batch);
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);

            batch.Draw(defaultTexture, new Rectangle(parent.position + position, size), Color.White);
        }
    }
}
