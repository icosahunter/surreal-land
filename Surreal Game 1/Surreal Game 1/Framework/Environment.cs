﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

// Debug entity-entity and entity-element collision.

namespace Surreal_Game_1
{
    public static class Environment
    {
        public static float gravity = 1f;
        public static int maxSpeed = 75;
        public static void Physics(List<Element> elements, Map map = null)
        {
            foreach(Entity e1 in elements.FindAll(x => x is Entity && x.topIsSolid == true))
            {
                e1.YSpeed += gravity;
                e1.position = e1.position.Add(e1.velocity);

                CheckForEntityTileCollisions(e1, map);

                foreach (Element e2 in elements.FindAll(x => !(x == e1 && x.topIsSolid == true)))
                {
                    if (e1.hitbox.CollidesWith(e2.hitbox))
                    {
                        if (e2 is Entity)
                        {
                            HandleEntityEntityCollision(e1, (Entity)e2);
                        }
                        else
                        {
                            HandleEntityStaticElementCollision(e1, e2);
                        }
                        
                    }
                }
            }
        }

        public static void Physics(IGameElement e1, IGameElement e2)
        {
            if (e1 is Entity && e1.topIsSolid)
            {
                if (e2 != e1 && e2.topIsSolid)
                {
                    if (e1.hitbox.CollidesWith(e2.hitbox))
                    {
                        //if (e2.topIsSolid && (e1.hitbox.bottom >= e2.hitbox.top))
                        //    ((Entity)e1).canJump = true;

                        if (e2 is IEntity)
                        {
                            HandleEntityEntityCollision((IEntity)e1, (IEntity)e2);
                        }
                        else
                        {
                            HandleEntityStaticElementCollision((IEntity)e1, e2);
                        }
                    }
                }
            }
        }

        public static void GravityCheck(IEntity e, Map map = null)
        {
            e.YSpeed += gravity;
            FixVelocity(e);
            e.position = e.position.Add(e.velocity);
            CheckForEntityTileCollisions(e, map);
        }

        /// <summary>
        /// Determines if an entity has exceeded the "terminal velocity" of the game
        /// </summary>
        /// <param name="e"> The entity in need of a velocity fix </param>
        public static void FixVelocity(IEntity e)
        {
            if (e.velocity.Length() > maxSpeed)
            {
                e.YSpeed = e.velocity.GetUnitVector().Y * maxSpeed;
                e.XSpeed = e.velocity.GetUnitVector().X * maxSpeed;
            }
        }

        /// <summary>
        /// Handle a collision between two Entities
        /// </summary>
        /// <param name="entity1"></param>
        /// <param name="entity2"></param>
        public static void HandleEntityEntityCollision(IEntity entity1, IEntity entity2)
        {
            Vector2 dir = entity1.hitbox.CollisionDirection(entity2.hitbox);

            // The position of Entity1 before it is fixed
            Point originalPosition = entity1.position;

            FixEntityPositionOnCollision(entity1, entity2.hitbox, dir);

            // The position of Entity1 after it is fixed
            Point newPosition = entity1.position;

            //Split the velocity vectors for each entity into their velocities parallel to and orthogonal to
            //the collision direction
            Vector2 e1OrthogonalV = entity1.velocity * dir.AbsoluteVal();
            Vector2 e1ParallelV = entity1.velocity - e1OrthogonalV;
            Vector2 e2OrthogonalV = entity2.velocity * dir.AbsoluteVal();
            Vector2 e2ParallelV = entity2.velocity - e2OrthogonalV;

            //handle momentum transfer
            Vector2 e1TransferV = ((float)entity2.mass * e2OrthogonalV * (float)entity2.material.hardness) / (float)entity1.mass;
            Vector2 e2TransferV = ((float)entity1.mass * e1OrthogonalV * (float)entity1.material.hardness) / (float)entity2.mass;

            //handle rebound
            Vector2 e1ReboundV = e1OrthogonalV * (float)entity1.material.elasticity * -1;
            Vector2 e2ReboundV = e2OrthogonalV * (float)entity2.material.elasticity * -1;

            //handle friction
            float frictionCoeff = (float)(entity1.material.roughness * entity2.material.roughness);

            Vector2 e1FrictionV = -1 * e1ParallelV.GetUnitVector() * e1OrthogonalV.Length() * frictionCoeff;
            Vector2 e2FrictionV = -1 * e2ParallelV.GetUnitVector() * e2OrthogonalV.Length() * frictionCoeff;

            if (e1FrictionV.Length() > e1ParallelV.Length())
            {
                e1FrictionV = -1 * e1ParallelV;
            }

            if (e2FrictionV.Length() > e2ParallelV.Length())
            {
                e2FrictionV = -1 * e2ParallelV;
            }

            //put them together for final velocites
            entity1.velocity = e1ReboundV + e1TransferV + e1ParallelV + e1FrictionV;
            entity2.velocity = e2ReboundV + e2TransferV + e2ParallelV + e2FrictionV;

            // Adjust the position of entity1 to account for the "missing" from the position adjustment
            entity1.position = entity1.position.Add(entity1.velocity.GetProjection((originalPosition - newPosition).ToVector2()));
        }

        /// <summary>
        /// Handle a collision between and entity and a static element
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="element"></param>
        public static void HandleEntityStaticElementCollision(IEntity entity, IGameElement element)
        {
            Vector2 dir = entity.hitbox.CollisionDirection(element.hitbox);

            // The position before it is fixed
            Point originalPosition = entity.position;

            FixEntityPositionOnCollision(entity, element.hitbox, dir);

            // The position after it is fixed
            Point newPosition = entity.position;

            //split the entities velocity into the pieces orthogonal and parallel to the
            //collision
            Vector2 OrthogonalV = entity.velocity * dir.AbsoluteVal();
            Vector2 ParallelV = entity.velocity - OrthogonalV;

            //handle rebound
            Vector2 ReboundV = OrthogonalV * (float)entity.material.elasticity * -1;

            //handle friction
            float frictionCoeff = (float)(entity.material.roughness * element.material.roughness);
            Vector2 FrictionV = -1 * ParallelV.GetUnitVector() * OrthogonalV.Length() * frictionCoeff;

            if (FrictionV.Length() > ParallelV.Length())
            {
                FrictionV = -1 * ParallelV;
            }

            //put it all together
            entity.velocity = ParallelV + ReboundV + FrictionV;

            // Adjust the position to account for the "missing" from the position adjustment
            entity.position = entity.position.Add(entity.velocity.GetProjection((originalPosition - newPosition).ToVector2()));
        }
        
        /// <summary>
        /// Move an entity outside the bounds of an element based on the collision direction
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="element"></param>
        /// <param name="collisionDir"></param>
        public static void FixEntityPositionOnElementCollision(IEntity entity, IGameElement element, Vector2 collisionDir)
        {
            if (collisionDir.X == -1)
            {
                FixEntityPositionOnCollision(entity, element.hitbox, collisionDir);
            }
            else if (collisionDir.X == 1)
            {
                entity.X = element.hitbox.left - (entity.hitbox.right - entity.hitbox.left);
            }
            if (collisionDir.Y == -1)
            {
                entity.Y = element.hitbox.bottom + 1;
            }
            else if (collisionDir.Y == 1)
            {
                entity.Y = element.hitbox.top - (entity.hitbox.bottom - entity.hitbox.top);
            }
            
            entity.hitbox.position = entity.position;
        }

        /// <summary>
        /// Check for and handle any collisions between the specified entity and tiles
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="map"></param>
        public static void CheckForEntityTileCollisions(IEntity entity, Map map)
        {
            if (map != null)
            {
                for (int i = 0; i < map.tileArray.GetLength(1); i++)
                {
                    for (int j = 0; j < map.tileArray.GetLength(0); j++)
                    {
                        Rectangle rect = map.GetTileBounds(i, j);
                        if (entity.hitbox.CollidesWith(rect))
                        {
                            Vector2 dir = entity.hitbox.CollisionDirection(rect);
                            HandleEntityTileCollision(entity, dir, i, j, map);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle the collision between an Entity and a specific tile
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="collisionDir"></param>
        /// <param name="X"> Tile X index </param>
        /// <param name="Y"> Tile Y index </param>
        /// <param name="map"></param>
        public static void HandleEntityTileCollision(IEntity entity, Vector2 collisionDir, int X, int Y, Map map)
        {
            bool hitSolidSide = CheckEntityTileCollision(X, Y, map, collisionDir);

            if (hitSolidSide)
            {
                // The bad position before adjusting (needed to fix position after collision)
                Point originalPosition = entity.position;
                Hitbox collisionTileHitbox = map.GetTileHitbox(X, Y);

                FixEntityPositionOnCollision(entity, collisionTileHitbox, collisionDir);

                // The adjusted position
                Point newPosition = entity.position;

                //split the entities velocity into components parallel to and orthogonal to the 
                //collision
                Vector2 OrthogonalV = entity.velocity * collisionDir.AbsoluteVal();
                Vector2 ParallelV = entity.velocity - OrthogonalV;

                //handle rebound
                Vector2 ReboundV = OrthogonalV * (float)entity.material.elasticity * -1;

                //handle friction
                float frictionCoeff = (float)(entity.material.roughness * map.GetTile(X, Y).material.roughness);
                Vector2 FrictionV =  -1 * ParallelV.GetUnitVector() * OrthogonalV.Length() * frictionCoeff;

                if (FrictionV.Length() > ParallelV.Length())
                {
                    FrictionV = -1 * ParallelV;
                }

                //put it all together
                entity.velocity = ParallelV + ReboundV + FrictionV;

                // Adjust the position to account for the "missing" from the position adjustment
                entity.position = entity.position.Add(entity.velocity.GetProjection((originalPosition - newPosition).ToVector2()));
            }
        }

        // TODO: COMMENT THIS APPROPRIATELY ANDREW!
        public static void FixEntityPositionOnCollision(IEntity entity, Hitbox collisionHitbox, Vector2 collisionDir)
        {
            bool priorCollision = true;
            bool collision = false;
            Vector2 velocity = entity.velocity;
            if (Math.Abs(velocity.X) < 1)
            {
                velocity.X = 0;
            }
            if (Math.Abs(velocity.Y) < 1)
            {
                velocity.Y = 0;
            }

            velocity = velocity / -2;

            int failsafe = 0;

            while(true)
            {

                entity.position = entity.position.Add(velocity);

                if (entity.hitbox.CollidesWith(collisionHitbox))
                {
                    collision = true;
                    if (velocity.X == 0 && collisionDir.Y == 0)
                    {
                        if (collisionDir.X == -1)
                        {
                            entity.SetHitboxPosition(new Point(collisionHitbox.right, entity.hitbox.position.Y));
                        }
                        else if (collisionDir.X == 1)
                        {
                            entity.SetHitboxPosition(new Point(collisionHitbox.position.X - entity.hitbox.width, entity.hitbox.position.Y));
                        }
                    }
                    if (velocity.Y == 0 && collisionDir.X == 0)
                    {
                        if (collisionDir.Y == -1)
                        {
                            entity.SetHitboxPosition(new Point(entity.hitbox.position.X, collisionHitbox.bottom));
                        }
                        else if (collisionDir.Y == 1)
                        {
                            entity.SetHitboxPosition(new Point(entity.hitbox.position.X, collisionHitbox.position.Y - entity.hitbox.height));                                
                        }
                    }
                }
                else
                {
                    collision = false;
                }

                if ((collision && !priorCollision) || (!collision && priorCollision))
                {
                    velocity = -velocity;
                }

                velocity = velocity / 2;

                if (Math.Abs(velocity.X) < 1 && velocity.X != 0)
                {
                    velocity.X = (velocity.X > 0) ? 1: -1;
                }
                if (Math.Abs(velocity.Y) < 1 && velocity.Y != 0)
                {
                    velocity.Y = (velocity.Y > 0) ? 1: -1;
                }

                if ((Math.Abs(velocity.X) <= 1 && Math.Abs(velocity.Y) <= 1) && priorCollision && !collision)
                {
                    break;
                }

                if ((Math.Abs(velocity.X) <= 1 && Math.Abs(velocity.Y) <= 1) && priorCollision == collision)
                {
                    failsafe++;
                    if (failsafe == 5)
                    {
                        break;
                    }
                }
                else
                {
                    failsafe = 0;
                }

                priorCollision = collision;
            }
        }

        /// <summary>
        /// Move an entity outside of the bounds of a tile based on the collision direction
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="X"> Tile X index </param>
        /// <param name="Y"> Tile Y index </param>
        /// <param name="map"></param>
        /// <param name="collisionDir"></param>
        /// <returns></returns>
        public static bool CheckEntityTileCollision(int X, int Y, Map map, Vector2 collisionDir)
        {
            bool hitSolidSide = false;
            if (map.GetTile(X, Y).rightIsSolid && collisionDir.X == -1)
            {
                hitSolidSide = true;
            }
            else if (map.GetTile(X, Y).leftIsSolid && collisionDir.X == 1)
            {
                hitSolidSide = true;
            }

            if (map.GetTile(X, Y).bottomIsSolid && collisionDir.Y == -1)
            {
                hitSolidSide = true;
            }
            else if (map.GetTile(X, Y).topIsSolid && collisionDir.Y == 1)
            {
                hitSolidSide = true;
            }

            return hitSolidSide;
        }
    }
}
