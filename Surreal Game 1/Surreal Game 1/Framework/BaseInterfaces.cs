﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public interface ISelfDrawable
	{
		void Draw(SpriteBatch batch);
	}

    public interface IAutoDrawable
    {
        void Draw(SpriteBatch batch, Point position, Point size);
    }

    public interface IUpdateable
    {
        void Update();
    }

    public interface IFormattable
    {
        Point size { get; set; }
        int width { get; set; }
        int height { get; set; }
        Point position { get; set; }
        int X { get; set; }
        int Y { get; set; }
        Point center { get; }
        Point upperLeft { get; }
        Point upperRight { get; }
        Point lowerLeft { get; }
        Point lowerRight { get; }
        int right { get; }
        int left { get; }
        int top { get; }
        int bottom { get; }
        Rectangle boundingRectangle { get; }
    }

    public interface IAutoFormattable
    {
        bool autoFormatting { get; set; }
        Point actualPosition { get;}
        int actualX { get; }
        int actualY { get; }
        Point actualSize { get;}
        int actualWidth { get; }
        int actualHeight { get; }
        Rectangle actualBoundingRectangle { get; }
    }

    public interface ICanHaveCamera
    {
        Camera camera { get; set; }
        bool cameraMode { get; set; }
    }

    public interface IUIElement : IUpdateable, IAutoDrawable, IAutoFormattable, ISelfDrawable, IFormattable
    {
        IUIElement parent { get; set; }
        List<IUIElement> children { get; }
        List<Trigger> triggers { get; }
        void AddTrigger(Trigger trigger);
        void RemoveTrigger(Trigger trigger);
        void AddChild(IUIElement element);
        void RemoveChild(IUIElement element);
    }

    public interface IGameElement : IUIElement, ICanHaveCamera
    {
        IUIElement image { get; set; }
        Hitbox hitbox { get; set; }
        Material material { get; set; }
        string description { get; set; }
        bool topIsSolid { get; set; }
        bool bottomIsSolid { get; set; }
        bool rightIsSolid { get; set; }
        bool leftIsSolid { get; set; }
        void SetHitboxPosition(Point position);
    }

    public interface IEntity : IGameElement
    {
        Vector2 velocity { get; set; }
        double YSpeed { get; set; }
        double XSpeed { get; set; }
        Vector2 momentum { get; }
        double XMomentum { get; }
        double YMomentum { get; }
        double mass { get; }
        double maxSpeed { get; set; }
        int damagePoints { get; set; }
        double facingDirection { get; set; }
        double velocityDirection { get; set; }
        bool isMovingUp { get; }
        bool isMovingDown { get; }
        bool isMovingRight { get; }
        bool isMovingLeft { get; }
        bool isFacingUp { get; }
        bool isFacingDown { get; }
        bool isFacingRight { get; }
        bool isFacingLeft { get; }
        bool isDestroyed { get; }
    }

    public interface IGameElementCollection : IUIElement, ICanHaveCamera
    {
        List<IGameElement> elements { get; }
    }
}
