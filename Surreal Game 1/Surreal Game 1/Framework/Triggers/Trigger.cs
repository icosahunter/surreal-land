﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public delegate bool Condition(Trigger t, object triggerOwner, object otherObj);
    public delegate void Result(Trigger t, object triggerOwner, object otherObj);

    public class Trigger
    {
        public Condition condition;
        public List<Result> results = new List<Result>();


        public Trigger(Condition condition_, Result result_)
        {
            condition = condition_;
            results.Add(result_);
        }

        public Trigger(Condition condition_, params Result[] results_)
        {
            condition = condition_;
            results = new List<Result>(results_);
        }

        public void Update(object triggerOwner, object otherObj)
        {
            if (condition(this, triggerOwner, otherObj))
            { 
                foreach(Result result in results)
                {
                    result(this, triggerOwner, otherObj);
                }
            }
        }
    }
}
