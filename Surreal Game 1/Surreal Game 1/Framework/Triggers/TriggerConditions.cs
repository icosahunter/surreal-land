﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public static class TriggerConditions
    {
        public static double defaultDistance = 100;

        public static Condition GameElementOnGround()
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner == otherObj)
                {
                    GameElement owner = triggerOwner as GameElement;
                    Map map = ((Room)owner.parent).map;
                    return map.GetTileFromPos(owner.center.X, owner.hitbox.bottom + 5).topIsSolid;
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition PlayerNearby(double distance = -1)
        {
            if (distance <= 0)
            {
                distance = defaultDistance;
            }

            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner is GameElement && otherObj is Player)
                {
                    GameElement owner = triggerOwner as GameElement;
                    GameElement other = otherObj as GameElement;
                    return owner.center.DistanceTo(other.center) < distance;
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition PlayerInteraction = TriggerConditions.AND(TriggerConditions.ButtonDown(InteractionTypes.INTERACT), TriggerConditions.PlayerNearby());

        public static Condition ButtonDown(object interaction)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner == otherObj)
                {
                    return InputController.IsButtonDown(interaction);
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition ButtonUp(object interaction)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner == otherObj)
                {
                    return InputController.IsButtonUp(interaction);
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition ButtonPressed(object interaction)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner == otherObj)
                {
                    return InputController.WasButtonPressed(interaction);
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition ButtonReleased(object interaction)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner == otherObj)
                {
                    return InputController.WasButtonReleased(interaction);
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition MouseOver()
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner == otherObj)
                {
                    UIElement owner = (UIElement)triggerOwner;
                    if (owner.actualBoundingRectangle.Contains(Inputs.MousePosition))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            };
        }

        public static Condition MouseDownOn()
        {
            return AND(MouseOver(), ButtonDown(InteractionTypes.CLICK));
        }

        //public static Condition MouseReleaseOn()
        //{
        //    return AND(MouseOver(), ButtonReleased(Inputs.MouseButton.LEFT));
        //}

        public static Condition MouseRightDownOn()
        {
            return AND(MouseOver(), ButtonDown(InteractionTypes.ALT_CLICK));
        }

        //public static Condition MouseRightReleaseOn()
        //{
        //    return AND(MouseOver(), ButtonReleased(Inputs.MouseButton.RIGHT));
        //}

        public static Condition AND(Condition condition1, Condition condition2)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                return condition1(t, triggerOwner, otherObj) && condition2(t, triggerOwner, otherObj);
            };
        }

        public static Condition OR(Condition condition1, Condition condition2)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                return condition1(t, triggerOwner, otherObj) || condition2(t, triggerOwner, otherObj);
            };
        }

    }
}
