﻿using Surreal_Game_1.GUI;
using Surreal_Game_1.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public static class TriggerResults
    {
        public static Result ChangeCurrentRoom(Room newRoom)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                GameElement owner = triggerOwner as GameElement;
                GameElement other = otherObj as GameElement;
                Room curRoom = owner.parent as Room;
                curRoom.Navigate(newRoom);
            };
        }

        public static Result AddItemToOtherObjInventory(Item item)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (otherObj is Element)
                {
                    //add item to triggerOwner.inventory
                }
            };
        }

        public static Result AddItemToTriggerOwnerInventory(Item item)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner is Element)
                {
                    //add item to triggerOwner.inventory
                }
            };
        }

        public static Result SendMessageToDialogueBox(string message, double priority, int timeout)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                DialogueController.SendMessage(message, priority, timeout);
            };
        }

        public static Result SendMessageToDialogueBoxUsingPlayerProximityPriority(string message, int timeout)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                if (triggerOwner is IUIElement && otherObj is IUIElement)
                {
                    IUIElement owner = triggerOwner as IUIElement;
                    IUIElement other = otherObj as IUIElement;
                    double d = owner.actualPosition.DistanceTo(other.actualPosition);
                    double priority = HelpfulMathFunctions.SimpleBellCurve(d);
                    DialogueController.SendMessage(message, priority, timeout);
                }
            };
        }

        public static Result RemoveTriggerOwnerFromRoom = 
        delegate (Trigger t, object triggerOwner, object otherObj)
        {
            if (triggerOwner is GameElement)
            {
                GameElement owner = triggerOwner as GameElement;
                Room curRoom = owner.parent as Room;
                curRoom.RemoveChild(owner);
            }
        };

        public static Result RemoveOtherObjFromRoom =
        delegate (Trigger t, object triggerOwner, object otherObj)
        {
            if (triggerOwner is GameElement)
            {
                GameElement other = otherObj as GameElement;
                Room curRoom = other.parent as Room;
                curRoom.RemoveChild(other);
            }
        };

        public static Result ActionResult(Action action)
        {
            return delegate (Trigger t, object triggerOwner, object otherObj)
            {
                action.Invoke();
            };
        }
    }
}
