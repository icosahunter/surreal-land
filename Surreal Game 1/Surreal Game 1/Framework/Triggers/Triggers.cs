﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Surreal_Game_1
{
    public static class Triggers
    {
        public static Trigger PlayerInteractionAlertTrigger(string interactPrompt)
        {
            return new Trigger(TriggerConditions.PlayerNearby(), TriggerResults.SendMessageToDialogueBoxUsingPlayerProximityPriority(interactPrompt, 1000));
        }

        public static Trigger PlayerInteractionPickUpTriggerOwnerAsItem(Item item)
        {
            return new Trigger(TriggerConditions.PlayerInteraction, TriggerResults.AddItemToOtherObjInventory(item), TriggerResults.RemoveTriggerOwnerFromRoom);
        }

        public static Trigger PlayerInteractionGoToNewRoom(Room room)
        {
            return new Trigger(TriggerConditions.PlayerInteraction, TriggerResults.ChangeCurrentRoom(room));
        }

    }
}
