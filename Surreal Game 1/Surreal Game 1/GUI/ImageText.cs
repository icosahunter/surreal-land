﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Surreal_Game_1
{
	public class ImageText : UIElement
    {
		string text;
		public ImageFont font;
        public int fontSize;
		public bool wrapAround = false;
		public int charSpace = 0;
		public int lineSpace = 0;
        List<IUIElement> charImages;    
		public string Text
		{
			get
			{
				return text;
			}

			set
			{
				text = value;
                charImages = font.StringToImageList(value);
			}
		}

		public ImageText(string str, ImageFont imgFont, int size, Point pos)
		{
			font = imgFont;
			fontSize = size;
			position = pos;
			Text = str;         
		}

		public ImageText(string str, ImageFont imgFont, int fontsize, int wrapWidth, Point pos)
        {
            font = imgFont;
            fontSize = fontsize;
            position = pos;
            size = new Point(wrapWidth, 0);
			wrapAround = true;
            Text = str;
        }
        
		public override void Draw(SpriteBatch batch)
        {
			int width;
			int lineNum = 0;
			int charXindex = 0;
			int currentWidth = 0;
			for (int i = 0; i < charImages.Count; i++)
			{
				width = (charImages[i].size.X * fontSize / charImages[i].size.Y);
				if (wrapAround && charXindex * width >= size.X)
				{
					lineNum++;
					charXindex = 0;
					currentWidth = 0;
				}
				currentWidth += width + charSpace;
				charImages[i].Draw(batch, position + new Point(currentWidth, lineNum * (fontSize + lineSpace)), new Point(width, fontSize));
				charXindex++;
			}
		}

		public override void Draw(SpriteBatch batch, Point pos, Point drawSize)
		{
			if (autoFormatting)
			{
				actualPosition = pos;
				actualSize = size;
			}
			else
			{
				actualPosition = position;
				actualSize = size;
			}

			int width;
			int lineNum = 0;
			int charXindex = 0;
			int currentWidth = 0;
			for (int i = 0; i < charImages.Count; i++)
			{
				width = (charImages[i].size.X * fontSize / charImages[i].size.Y);
				if (wrapAround && charXindex * width >= size.X)
				{
					lineNum++;
					charXindex = 0;
					currentWidth = 0;
				}
				currentWidth += width + charSpace;
				charImages[i].Draw(batch, actualPosition + new Point(currentWidth, lineNum * (fontSize + lineSpace)), new Point(width, fontSize));
				charXindex++;
			}
		}
	}
}
