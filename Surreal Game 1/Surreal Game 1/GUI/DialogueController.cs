﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Surreal_Game_1.GUI
{
    static class DialogueController
    {
        public static string currentMessage = "";
        public static double currentMessagePriority;
        public static int currentMessageTimeout;
        public static List<string> messageLog;
        public static Timer messageTimer = new Timer();

        static DialogueController()
        {
            messageTimer.Elapsed += delegate(Object source, ElapsedEventArgs e) { ClearCurrentMessage(); };
            messageTimer.AutoReset = false;
        }

        /// <summary>
        /// send message to be displayed in the dialog.
        /// priority can be used to prioritize based inversely on the distance of the 
        /// player to an object. Low frequency messages should generally have higher
        /// priority if hard coded priorities are necessary.
        /// </summary>
        /// <param name="message">message to send</param>
        /// <param name="priority">priority is a double, should be between 0 and 1</param>
        /// <param name="timeout">timeout of message in milliseconds, after which the current
        /// message will be cleared</param>
        /// <returns>boolean represents whether or not the message was accepted or rejected
        /// due to lower priority</returns>
        public static bool SendMessage(string message, double priority, int timeout)
        {
            if(priority > currentMessagePriority || message == currentMessage)
            {
                currentMessage = message;
                currentMessagePriority = priority;
                currentMessageTimeout = timeout;
                messageTimer.Interval = timeout;
                messageTimer.Start();
                return true;
            }
            return false;
        }

        public static void ClearCurrentMessage()
        {
            currentMessage = "";
            currentMessagePriority = 0;
            currentMessageTimeout = 0;
        }
    }
}
