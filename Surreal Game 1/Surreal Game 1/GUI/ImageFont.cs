﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Surreal_Game_1
{
	public class ImageFont
    {
		public Dictionary<char, IUIElement> characters = new Dictionary<char, IUIElement>();
		public Dictionary<char, IUIElement> escapeChars = new Dictionary<char, IUIElement>();
		public Dictionary<char, EscapeEffect> escapeEffects = new Dictionary<char, EscapeEffect>();
		public Point defaultWhiteSpaceSize = new Point(1, 1);
		public delegate void EscapeEffect(ImageFont font);
		public char escapeChar; 
        
        public List<IUIElement> StringToImageList(string text)
		{
			List<IUIElement> images = new List<IUIElement>();
			bool escape = false;
            foreach (char c in text)
			{
				if (c == escapeChar)
				{
                    if (escape)
					{
						if (characters.Keys.Contains(escapeChar))
						{
							images.Add(characters[escapeChar]);
						}
                        escape = false;
					}
					else
					{
						escape = true;
					}               
				}
				else
				{
					if (escape)
					{
						if (escapeEffects.Keys.Contains(c))
						{
							escapeEffects[c](this);
						}
						if (escapeChars.Keys.Contains(c))
						{
							images.Add(escapeChars[c]);
						}
					}
					else
					{
						if (characters.Keys.Contains(c))
						{
							images.Add(characters[c]);
						}
						else if (char.IsWhiteSpace(c))
						{
							images.Add(new EmptyImage(defaultWhiteSpaceSize));
						}
					}
				}
			}

			return images;
		}

		public void LoadFromSpriteSheet(string chars, Image fontSheet, Point charSize)
		{
			int cX = fontSheet.size.X / charSize.X;
			int cY = fontSheet.size.Y / charSize.Y;
			int count = 0;

			for (int i = 0; i < cY; i++)
			{
				for (int j = 0; j < cX; j++)
				{
					characters.Add(chars[count], new Image(fontSheet.defaultTexture, new Rectangle(j * charSize.X, i * charSize.Y, charSize.X, charSize.Y)));
					count++;
				}
			}
		}
	}
}
