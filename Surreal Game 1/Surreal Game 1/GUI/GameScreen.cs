﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class GameScreen : Screen
    {
        public Room currentRoom;
        public Camera camera;
        public GameScreen(Window window_) : base(window_)
        {
            camera = new Camera(this, new Point(0, 0));
        }

        public GameScreen(Window window_, Color background_) : base(window_, background_)
        {
            camera = new Camera(this, new Point(0, 0));
        }

        public override void Update()
        {
            base.Update();
            currentRoom.Update();
            camera.Update();
            foreach (IUIElement c1 in children)
            {
                foreach (IUIElement c2 in children)
                {
                    foreach (Trigger t in c1.triggers)
                    {
                        t.Update(c1, c2);
                    }
                }
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            window.game.GraphicsDevice.Clear(background);
            actualPosition = position;
            actualSize = size;

            currentRoom.Draw(batch);

            foreach (IUIElement e in children)
            {
                e.Draw(batch, actualPosition, actualSize);
            }
        }

        public override void Draw(SpriteBatch batch, Point pos, Point drawSize)
        {
            window.game.GraphicsDevice.Clear(background);
            if (autoFormatting)
            {
                actualPosition = pos;
                actualSize = drawSize;
            }
            else
            {
                actualPosition = position;
                actualSize = size;
            }

            currentRoom.Draw(batch, pos, drawSize);

            foreach (IUIElement e in children)
            {
                e.Draw(batch, actualPosition, actualSize);
            }
        }

    }
}
