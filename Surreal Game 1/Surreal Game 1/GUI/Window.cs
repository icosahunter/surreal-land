﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Window : IUpdateable
    {
        public Effect effect;
        public Screen currentScreen;
        public Game game;
        public GraphicsDeviceManager graphics;
        public SpriteBatch spritebatch;
        public RenderTarget2D renderTarget;
        int pixel = 1;
        int mult = 1;
        public int width
        {
            get
            {
                return graphics.PreferredBackBufferWidth;
            }
            set
            {
                graphics.PreferredBackBufferWidth = value;
                currentScreen.width = value;
                effect.Parameters["width"].SetValue(value);
                graphics.ApplyChanges();
            }
        }
        public int height
        {
            get
            {
                return graphics.PreferredBackBufferHeight;
            }
            set
            {
                graphics.PreferredBackBufferHeight = value;
                currentScreen.height = value;
                effect.Parameters["height"].SetValue(value);
                renderTarget = new RenderTarget2D(game.GraphicsDevice, width, height, false, game.GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.Depth24);
                graphics.ApplyChanges();
            }
        }

        public Window(Game game_, GraphicsDeviceManager graphics_)
        {
            game = game_;
            graphics = graphics_;
            currentScreen = new Screen(this);
            effect = game.Content.Load<Effect>("Pixelize");
            effect.Parameters["pixelation"].SetValue(1);
            renderTarget = new RenderTarget2D(game.GraphicsDevice, width, height, false, game.GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.Depth24);
            spritebatch = new SpriteBatch(game.GraphicsDevice);
        }

        public void Draw()
        {
            game.GraphicsDevice.SetRenderTarget(renderTarget);
            spritebatch.Begin(samplerState: SamplerState.PointClamp);
            currentScreen.Draw(spritebatch);
            spritebatch.End();

            game.GraphicsDevice.SetRenderTarget(null);
            spritebatch.Begin(samplerState: SamplerState.PointClamp, effect: effect);
            spritebatch.Draw(renderTarget, new Rectangle(0, 0, width, height), Color.White);
            spritebatch.End();
        }

        public void Update()
        {
            Inputs.Update();
            currentScreen.Update();
        }
    }
}
