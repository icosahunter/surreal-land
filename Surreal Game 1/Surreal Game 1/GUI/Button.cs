﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Button : UIElement
    {
        public IUIElement background 
        { 
            get 
            { 
                return bckgrnd; 
            } 
            set
            {
                value.autoFormatting = true;
                children.Remove(bckgrnd);
                children.Insert(0, value);
                bckgrnd = value;
            } 
        }
        private Trigger onClickTrigger;
        private IUIElement bckgrnd;

        public Button(IUIElement background_, Point position_, Point size_, Result onclick)
        {
            position = position_;
            size = size_;
            background = background_;
            onClickTrigger = new Trigger(TriggerConditions.MouseDownOn(), onclick);
            AddTrigger(onClickTrigger);
        }

        public void AddOnClick(Result onclick)
        {
            onClickTrigger.results.Add(onclick);
        }

        public void RemoveOnClick(Result onclick)
        {
            onClickTrigger.results.Remove(onclick);
        }
    }
}
