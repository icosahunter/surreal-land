﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class GridItem
    {
        public IUIElement item;
        public Point position;

        public GridItem(IUIElement item_, Point pos)
        {
            item = item_;
            position = pos;
        }
    }

    public class Grid : UIElement
    {
        public List<GridItem> items = new List<GridItem>();
        public IUIElement boxBackground;
        public IUIElement background;
        public int rows;
        public int columns;
        public override List<IUIElement> children
        {
            get
            {
                return items.Select(x => x.item).Union(base.children).ToList();
            }
        }

        public Grid(Point pos, int rows_, int columns_)
        {
            position = pos;
            size = new Point(rows_, columns_);
            rows = rows_;
            columns = columns_;
            background = new EmptyImage(size);
            boxBackground = new EmptyImage(new Point(10, 10));
            autoFormatting = true;
        }
        
        public Grid(Point pos, int rows_, int columns_, IUIElement background_)
        {
            position = pos;
            size = new Point(rows_, columns_);
            rows = rows_;
            columns = columns_;
            background = background_;
            boxBackground = new EmptyImage(new Point(10, 10));
            autoFormatting = true;
        }

        public Grid(Point pos, int rows_, int columns_, IUIElement background_, IUIElement boxBackground_)
        {
            position = pos;
            size = new Point(rows_, columns_);
            rows = rows_;
            columns = columns_;
            background = background_;
            boxBackground = boxBackground_;
            autoFormatting = true;
        }

        //TODO: Improve speed of finding empty slots in AppendItem method
        public void AppendItem(IUIElement item)
        {
            bool itemPlaced = false;
            for(int i = 0; i < rows && !itemPlaced; i++)
            {
                for(int j = 0; j < columns && !itemPlaced; j++)
                {
                    if (items.Where(x => x.position.X == j && x.position.Y == i).Count() == 0)
                    {
                        AddItem(item, new Point(j, i));
                        itemPlaced = true;
                    }
                }
            }
        }

        public void AddItem(IUIElement item)
        {
            items.Add(new GridItem(item, new Point(0, 0)));
        }

        public void AddItem(IUIElement item, Point pos)
        {
            items.Add(new GridItem(item, pos));
        }

        public override void Update()
        {
            base.Update();
            foreach(GridItem gi in items)
            {
                gi.item.Update();
            }
        }

        public override void Draw(SpriteBatch batch, Point pos, Point size_)
        {
            int boxH;
            int boxW;
            if (parent != null)
            {
                if (autoFormatting)
                {
                    background.Draw(batch, pos, size_);
                    boxH = size_.Y / rows;
                    boxW = size_.X / columns;
                }
                else
                {
                    background.Draw(batch, parent.position + position, size);
                    boxH = size.Y / rows;
                    boxW = size.X / columns;
                }
            }
            else
            {
                background.Draw(batch, position, size);
                boxH = size.Y / rows;
                boxW = size.X / columns;
            }

            foreach(GridItem item in items)
            {
                item.item.Draw(batch, new Point(item.position.X * boxW, item.position.Y * boxH), new Point(boxW, boxH));
                boxBackground.Draw(batch, new Point(item.position.X * boxW, item.position.Y * boxH), new Point(boxW, boxH));
            }
        }
    }
}
