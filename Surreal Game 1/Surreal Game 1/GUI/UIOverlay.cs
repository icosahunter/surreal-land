﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Surreal_Game_1.GUI;

namespace Surreal_Game_1
{
    public class UIOverlay : UIElement
    {
        ImageText dialogueBoxText;

        public UIOverlay(ImageFont font, int textSize) : base()
        {
            autoFormatting = true;
            dialogueBoxText = new ImageText("", font, textSize, new Point(100, 100));
            AddChild(dialogueBoxText);
        }

        public override void Update()
        {
            base.Update();
            dialogueBoxText.Text = DialogueController.currentMessage;
        }
    }
}
