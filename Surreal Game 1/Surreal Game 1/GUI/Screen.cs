﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Surreal_Game_1
{
    public class Screen : UIElement
    {
        public Color background = Color.Black;
        public Window window;

        public Screen(Window window_)
        {
            window = window_;
            position = new Point(0, 0);
            size = new Point(window.width, window.height);
        }

        public Screen (Window window_, Color background_)
        {
            background = background_;
            window = window_;
            position = new Point(0, 0);
            size = new Point(window.width, window.height);
        }

        public override void Draw(SpriteBatch batch)
        {
            window.game.GraphicsDevice.Clear(background);
            actualPosition = position;
            actualSize = size;

            foreach (IUIElement e in children)
            {
                e.Draw(batch, actualPosition, actualSize);
            }
        }

        public override void Draw(SpriteBatch batch, Point pos, Point drawSize)
        {
            window.game.GraphicsDevice.Clear(background);
            if (autoFormatting)
            {
                actualPosition = pos;
                actualSize = drawSize;
            }
            else
            {
                actualPosition = position;
                actualSize = size;
            }

            foreach (IUIElement e in children)
            {
                e.Draw(batch, actualPosition, actualSize);
            }
        }

        public void Navigate(Screen navigateTo)
        {
            window.currentScreen = navigateTo;
        }
    }
}
